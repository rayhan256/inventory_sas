<?php
namespace App\Helpers;

class IdGenerator {
    public static function generate($model, $trow, $length, $prefix){
        $data = $model::orderBy('id','desc')->first();
        if(!$data){
            $og_length = $length;
            $last_number = '';
        }else{
            $code = substr($data->$trow, strlen($prefix)+1);
            $actial_last_number = ($code/1)*1;
            $increment_last_number = ((int)$actial_last_number)+1;
            $last_number_length = strlen($increment_last_number);
            $og_length = $length - $last_number_length;
            $last_number = $increment_last_number;
        }
        $zeros = "";
        for($i=0;$i<$og_length;$i++){
            $zeros.="0";
        }
        return $prefix.'-'.$zeros.$last_number;
    }
  
    public static function generateFromDB($lastnum, $length, $prefix, $isBilyet = false){
        $actial_last_number = $lastnum - 1;
        $increment_last_number = ((int)$actial_last_number)+1;
        $last_number_length = strlen($increment_last_number);
        $og_length = $length - $last_number_length;
        $last_number = $increment_last_number;
            // $code = substr($data->$trow, strlen($prefix)+1);
        $zeros = "";
        for($i=0;$i<$og_length;$i++){
            $zeros.="0";
        }
        if ($isBilyet == false) {
            return $prefix.'-'.$zeros.$last_number;
        }
        return $zeros.$last_number;
    }
}
?>