<?php

namespace App\Helpers;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class FetchData {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function postRequest($url, $body = [])
    {
      $client = new Client();
      $response = null;
      try {
          $headers = [
            'headers' => [
              'access_token' => session('access_token'),
              'Authorization' => 'Bearer '.session('access_token')
            ],
            'form_params' => $body
          ];
        $request = $client->post($url, $headers);
        $response = json_decode($request->getBody()->getContents());
      }
      catch(\Exception $e) {
        $message = "External server call failed";
        Log::error($e);
        $error = $e->getResponse();

        if ($error == null) {
          $response = json_decode(json_encode(['code' => 500, 'message' => $message], JSON_FORCE_OBJECT));
        }
        else
        {
          $response = json_decode(json_encode(['code' => $error->getStatusCode(), 'message' => $error->getReasonPhrase()], JSON_FORCE_OBJECT));
        }
      }

      return $response;
    }

    public function postRequestMultipart($url, $body = [])
    {
      $client = new Client();
      $response = null;
      try {
          $headers = [
            'headers' => [
              'access_token' => session('access_token'),
              'Authorization' => 'Bearer '.session('access_token')
            ],
           $body
          ];
        $request = $client->post($url, $headers);
        $response = json_decode($request->getBody()->getContents());
      }
      catch(\Exception $e) {
        $message = "External server call failed";
        Log::error($e);
        $error = $e->getResponse();

        if ($error == null) {
          $response = json_decode(json_encode(['code' => 500, 'message' => $message], JSON_FORCE_OBJECT));
        }
        else
        {
          $response = json_decode(json_encode(['code' => $error->getStatusCode(), 'message' => $error->getReasonPhrase()], JSON_FORCE_OBJECT));
        }
      }

      return $response;
    }


    public function getRequest($url)
    {
      $client = new Client();
      $response = null;

      try
      {
        $request = $client->get($url);
        $response = json_decode($request->getBody()->getContents());
      } catch(\Exception $e)
      {
        $message = "External API call failed";
        Log::error($e);
        $error = $e->getResponse();
        if ($error == null) {
          $response = json_decode(json_encode(['code' => 500, 'message' => $message], JSON_FORCE_OBJECT));
        }
        else
        {
          $response = json_decode(json_encode(['code' => $error->getStatusCode(), 'message' => $error->getReasonPhrase()], JSON_FORCE_OBJECT));
        }

      }
      return $response;
    }

    public function putRequest($url, $body = [])
  {
    $client = new Client();
    $response = null;

    try
    {
      $headers = [
        'headers' => [
          'access_token' =>  session('access_token'),
          'Authorization' => 'Bearer '.session('access_token')
        ],
        'form_params' => $body
      ];

      if(count($body) == 0)
      {
        $request = $client->put($url, $headers);
      }
      else
      {
        $request = $client->put($url,  $headers);
      }

      $response = json_decode($request->getBody()->getContents());
    }
    catch(\Exception $e)
    {
      $message = "External server call failed";
      Log::error($e);
      $error = $e->getResponse();

      if ($error == null) {
        $response = json_decode(json_encode(['code' => 500, 'message' => $message], JSON_FORCE_OBJECT));
      }
      else
      {
        $response = json_decode(json_encode(['code' => $error->getStatusCode(), 'message' => $error->getReasonPhrase()], JSON_FORCE_OBJECT));
      }
    }

    return $response;
  }
}
