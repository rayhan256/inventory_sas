<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendingRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($title, $user, $data, $receiver)
    {
        //
        $this->title = $title;
        $this->user = $user;
        $this->data = $data;
        $this->receiver = $receiver;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.send_request_mail', [
            "title" => $this->title,
            "user" => $this->user,
            "data" => $this->data,
            "receiver" => $this->receiver]);
    }
}
