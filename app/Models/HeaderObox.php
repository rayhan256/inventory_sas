<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HeaderObox extends Model
{
    use HasFactory;
    protected $table = 'header_oboxes';
    protected $fillable = ['flag_header', 'ljk_type', 'ljk_code', 'datatype', 'period', 'data_count'];
}
