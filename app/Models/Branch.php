<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    use HasFactory;
    protected $table = 'branches';

    function debtors() {
        return $this->hasMany(Debtor::class);
    }

    function stocks() {
        return $this->hasMany(Stock::class);
    }

    function savingBook() {
        return $this->hasMany(SavingBook::class);
    }

    function bilyet() {
        return $this->hasMany(BilyetDeposit::class);
    }
}
