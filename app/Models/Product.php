<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $fillable = ['name', 'code', 'prefix'];

    function savingBooks()
    {
        return $this->hasMany(SavingBook::class);
    }

    function stocks()
    {
        return $this->hasMany(Stock::class);
    }

    function bilyet()
    {
        return $this->hasMany(BilyetDeposit::class);
    }

    function stock()
    {
        return $this->hasOne(Stock::class);
    }
}
