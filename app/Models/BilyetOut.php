<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BilyetOut extends Model
{
    use HasFactory;
    protected $table = 'bilyet_outs';
    protected $fillable = ['bilyet_deposit_id', 'debtor_id', 'stock_id', 'count'];

    function bilyet() {
        return $this->belongsTo(BilyetDeposit::class);
    }

    function debtor() {
        return $this->belongsTo(Debtor::class);
    }

    function stock() {
        return $this->belongsTo(Stock::class);
    }
}
