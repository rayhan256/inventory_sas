<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SavingBook extends Model
{
    use HasFactory;
    protected $table = 'saving_books';
    protected $fillable = ['code', 'product_id', 'branch_id', 'status', 'kpm_status'];

    function product() {
        return $this->belongsTo(Product::class);
    }

    function branch() {
        return $this->belongsTo(Branch::class);
    }

    function stockOut() {
        return $this->hasMany(StockOut::class);
    }

    function submission() {
        return $this->hasOne(Submission::class);
    }
}
