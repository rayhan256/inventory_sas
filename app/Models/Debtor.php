<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Debtor extends Model
{
    use HasFactory;
    protected $table = 'debtors';
    protected $fillable = ['account_id', 'name', 'address', 'city', 'kec', 'kel', 'postal_code', 'nik', 'phone', 'email', 'branch_id', 'province'];

    function branch() {
        return $this->belongsTo(Branch::class);
    }

    function book() {
        return $this->hasMany(StockOut::class);
    }
}
