<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockIn extends Model
{
    use HasFactory;
    protected $table = 'stock_ins';
    protected $fillable = ['count', 'date_in', 'product_id', 'stock_id', 'last_stock'];

    function product() {
        return $this->belongsTo(Product::class);
    }

    function stock() {
        return $this->belongsTo(Stock::class);
    }
}
