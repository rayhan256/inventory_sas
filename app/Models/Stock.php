<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    use HasFactory;
    protected $table = 'stocks';
    protected $fillable = ['last_stock', 'current_stock', 'product_id', 'branch_id'];

    function stockIn() {
        return $this->hasMany(StockIn::class);
    }

    function product() {
        return $this->belongsTo(Product::class);
    }

    function branch() {
        return $this->belongsTo(Branch::class);
    }

    function stockOut() {
        return $this->hasMany(StockOut::class);
    }
}
