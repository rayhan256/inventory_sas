<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KpmStockOut extends Model
{
    use HasFactory;
    protected $table = 'kpm_stock_outs';
    protected $fillable = ['count', 'date_out', 'branch_id', 'stock_id'];

    function branch() {
        return $this->belongsTo(Branch::class);
    }

    function stock() {
        return $this->belongsTo(Stock::class);
    }
}
