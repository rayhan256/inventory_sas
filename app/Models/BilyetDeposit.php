<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BilyetDeposit extends Model {
    use HasFactory;
    protected $table = "bilyet_deposits";
    protected $fillable = ['product_id', 'branch_id', 'code', 'status'];

    function branch() {
        return $this->belongsTo(Branch::class);
    }

    function product() {
        return $this->belongsTo(Product::class);
    }

    function submission() {
        return $this->hasMany(BilyetSubmission::class);
    }
}
