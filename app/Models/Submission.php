<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    use HasFactory;
    protected $table = 'submissions';
    protected $fillable = ['saving_book_id', 'submission_date', 'desc', 'file', 'status', 'submission_status'];

    function savingBook() {
        return $this->belongsTo(SavingBook::class);
    }
}
