<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BilyetSubmission extends Model
{
    use HasFactory;
    protected $table = 'bilyet_submissions';
    protected $fillable = ['bilyet_deposit_id', 'submission_date', 'desc', 'file', 'status', 'submission_status'];

    function bilyet() {
        return $this->belongsTo(BilyetDeposit::class, 'bilyet_deposit_id');
    }
}
