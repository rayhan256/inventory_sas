<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockOut extends Model
{
    use HasFactory;
    protected $table = 'stock_outs';
    protected $fillable = ['count', 'saving_book_id', 'debtor_id', 'stock_id', 'desc'];

    function savingBook() {
        return $this->belongsTo(SavingBook::class);
    }

    function debtor() {
        return $this->belongsTo(Debtor::class);
    }

    function stock() {
        return $this->belongsTo(Stock::class);
    }
}
