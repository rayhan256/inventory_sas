<?php

namespace App\Exports\Master;

use App\Models\Stock;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithProperties;

class GeneralReport implements FromCollection
{
   
    public function collection()
    {
        //
        $data = [];
        $reports = Stock::has('stockOut')->with(['stockOut.debtor', 'stockOut.savingBook'])->with('branch')->with('product')->get();
        foreach ($reports as $report) {
            foreach ($report->stockOut as $stock) {
                $data [] = [
                    "nama" => $stock->debtor->name,
                    'no_rekening' => $stock->debtor->account_id,
                    'nik' => $stock->debtor->nik,
                    'alamat' => $stock->debtor->address,
                    'kota' => $stock->debtor->city,
                    'kec' => $stock->debtor->kec,
                    'kel' => $stock->debtor->kel,
                    'postal_code' => $stock->debtor->postal_code,
                    'nomor_buku' => $stock->savingBook->code,
                    'produk_tabungan' => $report->product->name,
                ];
            }
        }

        return new Collection($data);
    }
}
