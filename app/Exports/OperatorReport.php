<?php

namespace App\Exports;

use App\Http\Controllers\Controller;
use App\Models\StockIn;
use App\Models\StockOut;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

class OperatorReport extends Controller implements FromCollection
{
    public function collection()
    {
        //
        $data = [];
        $reports = StockOut::with('savingBook.branch')->with('debtor')->whereRelation('debtor', 'branch_id', '=', $this->getUserBranchId())->get();
        foreach ($reports as $report) {
            $data [] = [
                'no_rekening' => $report->debtor->account_id,
                'nama_nasabah' => $report->debtor->name,
                'alamat' => $report->debtor->address,
                'nik' => $report->debtor->nik,
                'tanggal_keluar' => date('d-m-Y', strtotime($report->created_at)),
                'nomor_buku' => $report->savingBook->code
            ];
        }
        return new Collection($data);
    }
}
