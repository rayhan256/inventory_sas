<?php

namespace App\Http\Controllers\Operator;

use App\Http\Controllers\Controller;
use App\Models\BilyetDeposit;
use App\Models\BilyetSubmission;
use App\Models\Branch;
use Illuminate\Http\Request;

class BilyetSubmissionController extends Controller
{
    //
    public function index()
    {
        $data = [
            'bilyets' => BilyetDeposit::where('branch_id', $this->getUserBranchId())->get(),
        ];
        return view('users.submission.bilyet', $data);
    }

    public function postSubmission(Request $request)
    {
        $input = $request->all();
        $file = $request->file('file');

        if ($request->has('file')) {
            $filename = $file->getClientOriginalName();
            $file->move(public_path('uploads/submission'), $filename);

            BilyetSubmission::create([
                'bilyet_deposit_id' => $this->getBilyetByCode($input['bilyet_deposit_id']),
                'submission_date' => $input['submission_date'],
                'desc' => $input['desc'],
                'file' => $filename,
                'status' => $input['status'],
                'submission_status' => 1,
            ]);
            $notification = $this->sendWebNotification("Pengajuan Dari Cabang " . $this->getBranchName(), $input['desc']);
            // Mail::to("rayhangamawanto@gmail.com")->send(new SendingRequestMail());
            return back()->with('success', 'Pengajuan Berhasil Terkirim');
        }
    }

    // helper function
    private function getBilyetByCode($code)
    {
        $bilyetDeposits = BilyetDeposit::where('code', $code)->first();
        return $bilyetDeposits->id;
    }

    private function getBranchName()
    {
        $branch = Branch::where('id', $this->getUserBranchId())->first();
        return $branch->name;
    }
}
