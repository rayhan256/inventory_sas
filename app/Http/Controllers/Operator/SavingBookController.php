<?php

namespace App\Http\Controllers\Operator;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\SavingBook;
use Illuminate\Http\Request;

class SavingBookController extends Controller
{
    //
    function index(Request $request) {
        $productId = $request->input('product_id');
        if ($request->has('product_id')) {
            $savingBooks = SavingBook::with('product')->with('branch')->where('branch_id', $this->getUserBranchId())->where('product_id', $productId)->get();
        } else {
            $savingBooks = SavingBook::with('product')->with('branch')->where('branch_id', $this->getUserBranchId())->get();
        }
        return view('users.saving-books.index', ['books' => $savingBooks, 'products' => $this->getProduct()]);
    }

    private function getProduct() {
        return Product::all();
    }
}
