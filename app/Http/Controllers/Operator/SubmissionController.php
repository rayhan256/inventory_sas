<?php

namespace App\Http\Controllers\Operator;

use App\Http\Controllers\Controller;
use App\Mail\SendingRequestMail;
use App\Models\Branch;
use App\Models\SavingBook;
use App\Models\Submission;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SubmissionController extends Controller
{
    public function index()
    {
        $data = [
            'saving_books' => SavingBook::where('branch_id', $this->getUserBranchId())->get(),
        ];
        return view('users.submission.index', $data);
    }

    public function postSubmission(Request $request)
    {
        $request->validate(
            ['file' => 'required|mimes:pdf|max:1024'],
            [
                'mimes' => 'File harus bertipe PDF',
                'max' => 'Ukuran file harus dibawah atau sama dengan 1 mb',
            ]
        );
        $manopsAccount = User::where('role', 2)->get();
        $currentUser = User::where('id', Auth::user()->id)->with('detail.branch')->first();
        $input = $request->all();
        if ($request->has('file')) {
            $file = $request->file('file');
            $filename = $file->getClientOriginalName();
            $file->move(public_path('uploads/submission'), $filename);
            $submissionData = Submission::create([
                'saving_book_id' => $this->getSavingBookId($input['saving_book_id']),
                'submission_date' => $input['submission_date'],
                'desc' => $input['desc'],
                'file' => $filename,
                'status' => $input['status'],
                'submission_status' => 1,
            ]);
            // $notification = $this->sendWebNotification("Pengajuan Dari Cabang " . $this->getBranchName(), $input['desc']);
            foreach ($manopsAccount as $value) {
                Mail::to($value->email)->send(new SendingRequestMail("Laporan Bilyet Rusak", $currentUser, $input['saving_book_id'], $value->email));
            }
            // Mail::to("rayhangamawanto@gmail.com")->send(new SendingRequestMail("Laporan Bilyet Rusak", $currentUser, $input['saving_book_id'], "operasional@bprsas.co.id"));
            return redirect(route('operator-submission.index'))->with('success', 'Pengajuan Berhasil Dikirim');
        }
    }

    // helper function
    private function getSavingBookId($code)
    {
        $savingBook = SavingBook::where('code', $code)->first();
        return $savingBook->id;
    }

    private function getBranchName()
    {
        $branch = Branch::where('id', $this->getUserBranchId())->first();
        return $branch->name;
    }
}
