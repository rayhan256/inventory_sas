<?php

namespace App\Http\Controllers\Operator;

use App\Helpers\FetchData;
use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Debtor;
use Illuminate\Http\Request;

class DebtorController extends Controller
{
    //
    function index() {
        $debtors = Debtor::where('branch_id', $this->getUserBranchId())->get();
        $data = [];
        foreach ($debtors as $debtor) {
            $data[] =[
                'id' => $debtor->id,
                'name' => $debtor->name,
                'account_id' => $debtor->account_id,
                'address' => $debtor->address,
                'nik' => $debtor->nik,
            ]; 
        }
        return view('users.debtors.index', ['data' => json_decode(json_encode($data))]);
        // return response()->json($data);
    }

    function add() {
        return view('users.debtors.create', ['branch_id' => $this->getUserBranchId()]);
    }

    function post(Request $request) {
        $input = $request->all();
        Debtor::create($input);
        return redirect(route('operator-debtors.index'))->with('pesan', 'Nasabah Saved');
    }

    function editView($id) {
        $debtor = Debtor::find($id);
        return view('users.debtors.update', ['debtor' => $debtor, 'branch_id' => $this->getUserBranchId()]);
    }

    function edit(Request $request) {
        $input = $request->all();
        $debtor = Debtor::find($input['id']);
        $debtor->update($input);

        return redirect(route('operator-debtors.index'))->with('pesan', 'Nasabah Updated');
    }

    function destroy($id) {
        $debtor = Debtor::find($id);
        $debtor->delete();
        return back()->with('pesan', 'Nasabah Deleted');
    }
}
