<?php

namespace App\Http\Controllers\Operator;

use App\Exports\OperatorReport;
use App\Http\Controllers\Controller;
use App\Models\Stock;
use App\Models\StockOut;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    //
    function index() {
        $reports = Stock::has('stockOut')->with(['stockOut' => function($query) {
            $query->orderBy('created_at', 'desc');
        },'stockOut.debtor', 'stockOut.savingBook', 'branch', 'product'])->where('branch_id', $this->getUserBranchId())->latest()->get();
        return view('users.reports.index', ['reports' => $reports]);
        // return response()->json($reports);
    }

    function export() {
       return Excel::download(new OperatorReport, 'reports.xlsx');
    }
}
