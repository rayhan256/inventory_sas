<?php

namespace App\Http\Controllers\Operator;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    //
    function index() {
        $role = Auth::user()->role;
        switch ($role) {
            case 1:
                return view('users.dashboard', ['stocks' => $this->getStock(), 'branch_name' => $this->getBranchName()]);
                break;
            case 2:
                return redirect('/manops');
            default:
                return redirect('/master');
                break;
        }
    }

    private function getStock() {
        $stockCount = Stock::with('product')->where('branch_id', $this->getUserBranchId())->get();
        return $stockCount;
    }

    private function getBranchName() {
        $branch = Branch::where('id', $this->getUserBranchId())->first();
        return $branch->name;
    }
}
