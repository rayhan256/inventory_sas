<?php

namespace App\Http\Controllers\Operator;

use App\Http\Controllers\Controller;
use App\Models\SavingBook;
use App\Models\Stock;
use App\Models\StockIn;
use App\Models\StockOut;
use App\Models\Submission;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ExportPdfController extends Controller
{
    //
    function index() {
        $data = [
            'last_stock' => $this->getbranchStock()->sum('last_stock'),
            'current_stock' => $this->getbranchStock()->sum('current_stock'),
            'broken_stock' => $this->getSubmissionResult(0),
            'lost_stock' => $this->getSubmissionResult(1),
            'stock_out' => $this->getTotalStock('out'),
            'stock_in' => $this->getTotalStock('in'),
            'last_stock_bilyet' => $this->getbranchStock('bilyet')->sum('last_stock'),
            'current_stock_bilyet' => $this->getbranchStock('bilyet')->sum('current_stock'),
            'broken_stock_bilyet' => $this->getSubmissionResult(0, 'bilyet'),
            'lost_stock_bilyet' => $this->getSubmissionResult(1, 'bilyet'),
            'stock_out_bilyet' => $this->getTotalStock('out', 'bilyet'),
            'stock_in_bilyet' => $this->getTotalStock('in', 'bilyet'),
            'date' => Carbon::now()->locale('id')->settings(['formatFunction' => 'translatedFormat'])->format('d F Y'),
            'gelegar_h' => $this->getBookCode(3)->code,
            'gelegar_l' => $this->getBookCode(3, 'lowest')->code,
            'tamara_h' => $this->getBookCode(1)->code ?? null,
            'tamara_l' => $this->getBookCode(1, 'lowest')->code ?? null,
            'tabunganku_h' => $this->getBookCode(2)->code,
            'tabunganku_l' => $this->getBookCode(2, 'lowest')->code,
            'taubah_h' => $this->getBookCode(4)->code,
            'taubah_l' => $this->getBookCode(4, 'lowest')->code,
            'bilyet_h' => $this->getBookCode(5)->code ?? null,
            'bilyet_l' => $this->getBookCode(5, 'lowest')->code ?? null
        ];
        return view('users.pdf.berita_acara', $data);
        // return response()->json($data);
    }

    private function getbranchStock($status = "butab") {
        if ($status == 'butab') {
            $stock = Stock::where('branch_id', $this->getUserBranchId())->where('product_id', '!=', 5)->get();
        } else {
            $stock = Stock::where('branch_id', $this->getUserBranchId())->where('product_id', 5)->get();
        }
        return $stock;
    }

    private function getDynamicStockId($branchId) {
        $stockId = Stock::query()->where('branch_id', $branchId)->first();
        return $stockId->id;
    }

    private function getSubmissionResult($status = 0, $product = 'butab') {
       if ($product == 'butab') {
        return Submission::with(['savingBooks' => function(Builder $query) {
            $query->where('product_id', '!=', 5);
        }])->where('status', $status)->count();
       } else {
        return Submission::with(['savingBooks' => function(Builder $query) {
            $query->where('product_id', 5);
        }])->where('status', $status)->count();
       }
    }

    private function getTotalStock($status = 'in', $product = 'butab') {
       if ($product == 'butab') {
           $stock = Stock::withSum('stockIn', 'count')->withSum('stockOut', 'count')->where('branch_id', $this->getUserBranchId())->where('product_id', '!=', 5)->get();
       } else {
        $stock = Stock::withSum('stockIn', 'count')->withSum('stockOut', 'count')->where('branch_id', $this->getUserBranchId())->where('product_id',5)->get();
       }
        $arr_stockIn = [];
        $arr_stockOut = [];
        foreach ($stock as $item) {
            # code...
            array_push($arr_stockIn, $item->stock_in_sum_count);
            array_push($arr_stockOut, $item->stock_out_sum_count);
        }
        // $stockIn = StockIn::where('branch_id', $stockId->branch_id)->get();
       if ($status == 'in') {
           return array_sum($arr_stockIn);
       } else {
           return array_sum($arr_stockOut);
       }
    }

    private function getStockOut() {
        $stockOut = StockOut::where('branch_id', $this->getUserBranchId())->get();
        return $stockOut;
    }

    private function getBookCode($product_id, $sortBy = "highest") {
        switch ($sortBy) {
            case 'lowest':
                $bookCode = SavingBook::where('product_id', $product_id)->where('branch_id', $this->getUserBranchId())->orderBy('id', 'asc')->first();
                return $bookCode;
                break;
            default:
                # code...
                $bookCode = SavingBook::where('product_id', $product_id)->where('branch_id', $this->getUserBranchId())->orderBy('id', 'desc')->first();
                return $bookCode;
                break;
        }
    }
}
