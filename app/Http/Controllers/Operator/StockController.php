<?php

namespace App\Http\Controllers\Operator;

use App\Http\Controllers\Controller;
use App\Models\BilyetDeposit;
use App\Models\BilyetOut;
use App\Models\Debtor;
use App\Models\Product;
use App\Models\SavingBook;
use App\Models\Stock;
use App\Models\StockOut;
use Illuminate\Http\Request;

class StockController extends Controller
{

    function index()
    {
        $data = [
            'products' => $this->getProducts(),
            'saving_books' => $this->getSavingBookByBranch(),
            'debtors' => $this->getDebtorsByBranch(),
            'stocks' => Stock::query()->where('branch_id', $this->getUserBranchId())->with('product')->get(),
            'upper' => $this->savingBookUpper('3'),
            'bottom' => $this->savingBookBottom('3')
        ];
        return view('users.stocks.index', $data);
        // return response()->json($data);
    }

    function generateStock()
    {
        $stock = Stock::query()->where('branch_id', $this->getUserBranchId())->with('product')->get();
        return view('users.stocks.generate-stocks', ['stocks' => $stock, 'products' => $this->getProducts()]);
        // return response()->json($stock);
    }

    function postMasterStock(Request $request)
    {
        $input = $request->all();
        Stock::create([
            'last_stock' => $input['last_stock'],
            'current_stock' => $input['current_stock'],
            'product_id' => $input['product_id'],
            'branch_id' => $this->getUserBranchId()
        ]);
        return back()->with('success', 'Master Stock Saved');
    }


    function postStockOut(Request $request)
    {
        $input = $request->all();
        $savingBook = SavingBook::where('branch_id', $this->getUserBranchId())->where('code',  $input['saving_book_id'])->first();
        $debtor = Debtor::where('nik', $input['debtor_id'])->where('account_id', $input['account_id'])->where('branch_id', $this->getUserBranchId())->first();
        StockOut::create([
            'count' => 1,
            'saving_book_id' => $savingBook->id,
            'debtor_id' => $debtor->id,
            'stock_id' => $this->getDynamicStockId($input['product_id']),
            'desc' => $input['desc']
        ]);
        $this->decreaseStock($this->getDynamicStockId($input['product_id']));
        $update = $this->updateStatusSavingBook($savingBook->id);
        return back()->with('success', 'Stock Out Submitted');
    }

    function postManualStockOut(Request $request)
    {
        $input = $request->all();
        $savingBook = SavingBook::where('code', $input['saving_book_id'])->first();
        $splitProduct = explode("-", $input['saving_book_id']);
        $productPrefix = count($splitProduct) > 1 ? $splitProduct[0] : null;
        $debtor = Debtor::where('nik', explode(' - ', $input['debtor_id'])[0])->where('account_id', explode(' - ', $input['debtor_id'])[1])->first();
        StockOut::create([
            'count' => 1,
            'saving_book_id' => $savingBook->id,
            'debtor_id' => $debtor->id,
            'stock_id' => $this->getDynamicStockId($productPrefix),
            'desc' => $input['desc']
        ]);

        $this->decreaseStock($this->getDynamicStockId($productPrefix));
        $this->updateStatusSavingBook($savingBook->id);
        return back()->with('success', 'Stock Out Submitted');
    }

    function postBilyetOut(Request $request)
    {
        $input = $request->all();
        $bilyet = BilyetDeposit::where('code', $input['bilyet_id'])->first();
        $debtor = Debtor::where('account_id', $input['debtor_id'])->first();

        BilyetOut::create([
            'count' => 1,
            'bilyet_deposit_id' => $bilyet->id,
            'debtor_id' => $debtor->id,
            'stock_id' => $this->getDynamicStockId(null)
        ]);

        $this->decreaseStock($this->getDynamicStockId(null));
        $update = $this->updateBilyetStatus($bilyet->id);
        return back()->with('success', 'Stock Out Submitted');
    }

    function getSavingBookBranch($product_id)
    {
        $savingBook = SavingBook::where('branch_id', $this->getUserBranchId())->where('product_id',  $product_id)->first();
        return response()->json($savingBook);
    }

    function getDebtorByNik($nik)
    {
        $debtor = Debtor::where('branch', $this->getUserBranchId())->where('nik', $nik)->first();
        return response()->json($debtor);
    }


    // Helper Function
    private function savingBookUpper($productId)
    {
        $savingBook = SavingBook::where('product_id', $productId)->where('branch_id', $this->getUserBranchId())->orderBy('id', 'asc')->first();
        return $savingBook;
    }

    private function savingBookBottom($productId)
    {
        $savingBook = SavingBook::where('product_id', $productId)->where('branch_id', $this->getUserBranchId())->orderBy('id', 'desc')->first();
        return $savingBook;
    }

    private function decreaseStock($id)
    {
        $stock = Stock::find($id);
        $stock->update([
            'last_stock' => $stock->current_stock,
            'current_stock' => $stock->current_stock - 1
        ]);
    }

    private function getProducts()
    {
        return Product::all();
    }

    private function getSavingBookByBranch()
    {
        $savingBooks = SavingBook::where('branch_id', $this->getUserBranchId())->where('status', 0)->get();
        return $savingBooks;
    }

    private function getDebtorsByBranch()
    {
        $debtors = Debtor::where('branch_id', $this->getUserBranchId())->get();
        return $debtors;
    }

    private function getDynamicStockId($productId)
    {
        $product = $productId == null ? Product::where('code', '05')->first() : Product::where('prefix', $productId)->first();
        $stockId = Stock::query()->where('product_id', $product->id)->where('branch_id', $this->getUserBranchId())->first();
        return $stockId->id;
    }

    private function updateStatusSavingBook($request)
    {
        $savingBook = SavingBook::find($request);
        $savingBook->update([
            'status' => 1
        ]);
        return $savingBook;
    }

    private function updateBilyetStatus($request)
    {
        $bilyet = BilyetDeposit::find($request);
        $bilyet->update([
            'status' => 1
        ]);
        return $bilyet;
    }
}
