<?php

namespace App\Http\Controllers;

use App\Models\BilyetDeposit;
use App\Models\DeviceToken;
use App\Models\SavingBook;
use App\Models\Stock;
use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function getUserId()
    {
        return Auth::user()->id;
    }

    protected function getUserBranchId()
    {
        $userId = $this->getUserId();
        $data = UserDetail::where('user_id', $userId)->first();
        return $data->branch_id;
    }

    protected function getSavingStock($product)
    {
        $count = Stock::where('branch_id', $this->getUserBranchId())->where('product_id', $product)->get()->count();
        return $count;
    }

    protected function getBranchSavingBook()
    {
        $savingBook = SavingBook::where('branch_id', $this->getBranchId())->get();
        return $savingBook;
    }

    protected function getBilyetStockByBranch()
    {
        $bilyets = BilyetDeposit::where('branch_id', $this->getUserBranchId())->where('status', 0)->get();
        return $bilyets;
    }

    private function getBranchId()
    {
        $userId = $this->getUserId();
        $user = User::with('detail')->where('id', $userId)->first();
        return $user->detail->branch_id;
    }

    protected function getBranchTotalStocks($branchId, $productId)
    {
        $count = Stock::where('product_id', $productId)->where('branch_id', $branchId)->get();
        return $count;
    }

    protected function sendWebNotification($title, $desc)
    {
        // $userId = Auth::user()->id;
        $url = 'https://fcm.googleapis.com/fcm/send';
        $FcmToken = DeviceToken::whereNotNull('device_token')->pluck('device_token')->all();
        $serverKey = 'AAAAXU2C1C4:APA91bG7IlHwTp34FKvJP9-lGzbVljUe6wrRanjK_1rWKkBffCheR8tC-F9yLtaVPoronhu1rANr3dSCulI2jwbrV78-RL066owurpbb430_ulYqyWMGcK9qfTHjv7TPzFiWnjQVT6hn';

        $data = [
            "registration_ids" => $FcmToken,
            "notification" => [
                "title" => $title,
                "body" => $desc,
            ]
        ];
        $encodedData = json_encode($data);

        $headers = [
            'Authorization:key=' . $serverKey,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
            return false;
        }

        // Close connection
        curl_close($ch);

        return true;
    }

    protected function generateSavingBookCode($prefix, $productId, $isDeposit = false)
    {
        $savingBooks = SavingBook::orderBy('id', 'desc')->where('product_id', $productId)->first();
        if (!$savingBooks) {
            switch ($prefix) {
                case 'TAB':
                    $code = $isDeposit == false ? $prefix . "-000000" : "023391";
                    break;
                case 'TAM':
                    $code = $isDeposit == false ? $prefix . "-000000" : "023391";
                    break;
                case 'TAU':
                    $code = $isDeposit == false ? $prefix . "-000000" : "023391";
                    break;
                case 'SIG':
                    $code = $isDeposit == false ? $prefix . "-000000" : "023391";
                    break;
                default:
                    if ($isDeposit == true) {
                        $code = "023391";
                    } else {
                        $code =  $prefix . "-000000";
                    }
                    break;
            }
        } else {
            $code = $savingBooks->code;
        }
        if ($isDeposit == true) {
            # code...
            $code = ltrim($code, '0');
            return $code;
        } else {
            $code = substr($code, 4, 6);
            $code = ltrim($code, '0');
            return $code;
        }
    }
}
