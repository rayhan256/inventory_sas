<?php

namespace App\Http\Controllers;

use App\Models\DeviceToken;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WebNotificationController extends Controller
{
    function storeToken(Request $request) {
        $userId = Auth::user()->id;
        $deviceToken = DeviceToken::create([
            'device_token' => $request->device_token,
            'user_id' => $userId
        ]);
        return response()->json(['Token Stored To DB']);
    }

}
