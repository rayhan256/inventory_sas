<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Debtor;
use Illuminate\Http\Request;

class DebtorController extends Controller
{

    function index(Request $request) {
        if ($request->has('branch_id')) {
            $debtors = Debtor::with('branch')->where('branch_id', $request['branch_id'])->get();
        } else {
            $debtors = Debtor::with('branch')->get();
        }

        return view('master.debtors.index', ['debtors' => $debtors, 'branches' => Branch::all()]);
    }

    function editView($id) {
        $debtor = Debtor::find($id);
        return view('master.debtors.update', ['debtor' => $debtor, 'branch_id' => $debtor->branch_id]);
    }

    function edit(Request $request) {
        $input = $request->all();
        $debtor = Debtor::find($input['id']);
        $debtor->update($input);

        return redirect(route('master-debtor.index'))->with('pesan', 'Nasabah Updated');
    }

    function destroy($id) {
        $debtor = Debtor::find($id);
        $debtor->delete();
        return back()->with('pesan', 'Nasabah Deleted');
    }
}
