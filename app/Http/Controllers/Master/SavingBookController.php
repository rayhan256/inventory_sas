<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\SavingBook;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SavingBookController extends Controller
{
    //
    public function index(Request $request)
    {
        if ($request->has('branch_id') || isset($request->query()['branch_id'])) {
            $books = SavingBook::with('branch')->with('product')->where('branch_id', $request['branch_id'])->paginate(10)->withQueryString();
        } else {
            $books = SavingBook::with('branch')->with('product')->paginate(10);
        }

        return view('master.saving_stocks.index', ['books' => $books, 'branches' => Branch::all()]);
    }
}
