<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Product;
use App\Models\Stock;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    //
    public function index()
    {
        $products = Product::all();
        $branches = Branch::with(['stocks.product'])->get();
        return view('master.product.index', ['products' => $products, 'branches' => $branches]);
    }

    public function create()
    {
        return view('master.product.create');
    }

    public function insertProduct(Request $request)
    {
        $input = $request->all();
        Product::create([
            'name' => $input["name"],
            'code' => $input["code"],
            'prefix' => $input["prefix"]
        ]);
        return back()->with('success', 'Produk berhasil ditambahkan');
    }

    public function branchDetail($id)
    {
        $products = Product::all();
        $branch = Branch::where('id', $id)->with('stocks.product')->first();
        return view('master.product.branch_detail', ['branch' => $branch, 'products' => $products]);
    }

    public function storeBranchProduct(Request $request)
    {
        Stock::create([
            'current_stock' => 0,
            'last_stock' => 0,
            'product_id' => $request->product_id,
            'branch_id' => $request->branch_id
        ]);

        return redirect(route('master-product.index'))->with('success', 'berhasil setting stock produk');
    }
}
