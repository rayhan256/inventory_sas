<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\BilyetDeposit;
use App\Models\BilyetSubmission;
use App\Models\SavingBook;
use App\Models\Stock;
use App\Models\Submission;
use Illuminate\Http\Request;

class AprovalController extends Controller
{
    //
    function handleSavingBookSubmission($id, $savingBookId) {
        $savingBook = SavingBook::with(['product.stock'])->where('id', $savingBookId)->first();
        $submission = Submission::find($id);
        $savingBook->update([
            'status' => $this->generateStatusCode($submission->status)
        ]);
        $submission->update([
            'submission_status' => 0
        ]);
        $decreaseStock = $this->decreaseStock($savingBook->product->stock->id);
        // return response()->json(['savingBook' => $savingBook, 'submission' => $submission, 'stock' => $stock]);
        return back()->with('success', 'Submission Approved');
    }

    function handleBilyetSubmission($id, $bilyetId) {
        $bilyet = BilyetDeposit::with('product.stock')->where('id', $bilyetId)->first();
        $submission = BilyetSubmission::find($id);

        $bilyet->update([
            'status' => $this->generateStatusCode($submission->status)
        ]);
        $submission->update([
            'submission_status' => 0
        ]);
        $decreaseStock = $this->decreaseStock($bilyet->product->stock->id);
        return back()->with('success', 'Submission Approved');
    }

    private function generateStatusCode($statusCode) {
        switch ($statusCode) {
            case 0:
                $statusCode = 3;
                break;
            case 1:
                $statusCode = 2;
                break;
            default:
                $statusCode = 3;
                break;
        }
        return $statusCode;
    }

    private function decreaseStock($id) {
        $stock = Stock::find($id);
        $stock->update([
            'last_stock' => $stock->current_stock,
            'current_stock' => $stock->current_stock - 1
        ]);
    }
}
