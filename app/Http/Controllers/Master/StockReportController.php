<?php

namespace App\Http\Controllers\Master;

use App\Exports\Master\GeneralReport;
use App\Http\Controllers\Controller;
use App\Models\SavingBook;
use App\Models\Stock;
use App\Models\StockOut;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class StockReportController extends Controller
{
    //
    function index()
    {
        $reports = Stock::has('stockOut')->with(['stockOut.debtor', 'stockOut.savingBook', 'branch', 'product'])->orderBy('created_at', 'desc')->get();
        return view('master.reports.index', ['reports' => $reports]);
    }

    function detail($id)
    {
        $report = StockOut::with(['debtor', 'savingBook'])->where('id', $id)->first();
        $data = [
            'report' => $report,
            'saving_books' => SavingBook::where('branch_id', $report->savingBook->branch_id)->where('status', 0)->get(),
        ];
        return view('master.reports.update', $data);
    }

    function edit(Request $request)
    {
        // Ambil Inputan
        $input = $request->all();

        // Ambil Data
        $savingBook = SavingBook::where('code', $input['saving_book_id'])->first();
        $stockId = $this->getDynamicStockId($savingBook->product_id, $savingBook->branch_id);

        // Deklarasi Model
        $book = SavingBook::find($savingBook->id);
        $stock = Stock::find($stockId);

        $oldBook = SavingBook::where('code', $input['old_book_id'])->first();
        $oldStock = Stock::find($this->getDynamicStockId($oldBook->product_id, $oldBook->branch_id));
        $stockOut = StockOut::find($input['id']);

        // eksekusi
        $oldBook->status = 0;
        $book->status = 1;
        $stockOut->saving_book_id = $book->id;

        // handle manage stock
        if ($book->product_id == $oldBook->product_id) {
            $stock->current_stock = $stock->current_stock - 1;
            // $stock->last_stock = $stock->current_stock;
            $stock->current_stock = $stock->current_stock + 1;
        } else {
            $stock->current_stock = $stock->current_stock - 1;
            $oldStock->current_stock = $oldStock->current_stock + 1;
            // $stock->last_stock = $stock->current_stock;
        }

        $oldBook->save();
        $book->save();
        $stockOut->save();
        $stock->save();
        $oldStock->save();
        // return
        return back()->with('success', 'Data Berhasil Dirubah');
        // return response()->json(['book' => $book, 'stock' => $stock, 'old_book' => $oldBook, 'stock_out' => $stockOut, 'stock' => $stock]);
    }

    private function getAllSavingBook()
    {
        $data = SavingBook::where('status', 0)->get();
        return $data;
    }

    function exportGeneralReport()
    {
        return Excel::download(new GeneralReport, 'general_reports.xlsx', null, [
            "nama",
            'no_rekening',
            'nik',
            'alamat',
            'kota',
            'kec',
            'kel',
            'postal_code',
            'nomor_buku',
            'produk_tabungan',
        ]);
    }

    private function getDynamicStockId($productId, $branchId)
    {
        $stockId = Stock::query()->where('product_id', $productId)->where('branch_id', $branchId)->first();
        return $stockId->id;
    }
}
