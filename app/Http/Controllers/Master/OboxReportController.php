<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\HeaderObox;
use App\Models\Stock;
use Illuminate\Http\Request;

class OboxReportController extends Controller
{
   function index() {
       $data = [
           'header' => HeaderObox::all(),
           'saving_book' => Stock::with('product')->with('branch')->with('stockIn')->where('product_id', '!=', '5')->get(),
           'branch' => Branch::all(),
           'deposits_stock' => Stock::with('product')->with('branch')->where('product_id', 5)->first(),
           'saving_book_stock' => Stock::with('product')->with('branch')->where('product_id', '!=', '5')->sum('current_stock'),
       ];
    //    return view('master.reports.obox', $data);
        return response()->json($data);
   }

   function postOboxHeaderTable(Request $request) {
       $input = $request->all();
       HeaderObox::create($input);
       return back()->with('success', 'header table setup successfully');
   }
}
