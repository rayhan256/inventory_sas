<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\SavingBook;
use App\Models\Stock;
use App\Models\Submission;

class DashboardController extends Controller
{
    //
    function index() {
        $data = [
            'kpm' => $this->getBranchTotalStock(1),
            'kpo' => $this->getBranchTotalStock(2),
            'pemalang' => $this->getBranchTotalStock(3),
            'brangsong' => $this->getBranchTotalStock(4),
            'mranggen' => $this->getBranchTotalStock(5),
            'ambarawa' => $this->getBranchTotalStock(6),
            'semarang' => $this->getBranchTotalStock(7),
            'submissions' => Submission::with('savingBook.branch')->get(),
        ];
        return view('master.dashboard', $data);
        // return response()->json($data);
    }

    private function getBranchTotalStock($branchId) {
        $totalStocks = SavingBook::where('branch_id', $branchId)->where('soft_deletes', 1)->where('kpm_status', 1)->count();
        return $totalStocks ?? 0;
    }
}
