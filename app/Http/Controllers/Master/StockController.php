<?php

namespace App\Http\Controllers\Master;

use App\Helpers\IdGenerator;
use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\KpmStockOut;
use App\Models\Product;
use App\Models\SavingBook;
use App\Models\Stock;
use App\Models\StockIn;
use Illuminate\Http\Request;

class StockController extends Controller
{
    function index(Request $request)
    {
        $data = [
            'kpm' => $this->getBranchTotalStock(1),
            'kpo' => $this->getBranchTotalStock(2),
            'pemalang' => $this->getBranchTotalStock(3),
            'brangsong' => $this->getBranchTotalStock(4),
            'mranggen' => $this->getBranchTotalStock(5),
            'ambarawa' => $this->getBranchTotalStock(6),
            'semarang' => $this->getBranchTotalStock(7),
            'products' => Product::all(),
            'branches' => Branch::all(),
            'saving_books' => $this->getSavingBookByProduct(),
            'saving_book_details' => Stock::with('product')->with('branch')->get(),

        ];
        return view('master.stock-controls.index', $data);
        // return response()->json($data);
    }

    function postStockIn(Request $request)
    {
        $input = $request->all();
        if ($input['book_code_lowest'] != null && $input['book_code_highest'] != null) {
            $codeLowest = $this->getCode($input['book_code_lowest'], true);
            $codeHighest = $this->getCode($input['book_code_highest'], true);
            $listCode = [];
            for ($i = (int)$codeLowest; $i <= (int)$codeHighest; $i++) {
                array_push($listCode, IdGenerator::generateFromDB($i, 6, $this->generatePrefixCode((int)5), true));
            }
            if (count($listCode) == $input['count']) {
                for ($i = (int)$codeLowest; $i <= (int)$codeHighest; $i++) {
                    SavingBook::create(['code' => IdGenerator::generateFromDB($i, 6, $this->generatePrefixCode((int)5), true), 'product_id' => $input['product_id'], 'branch_id' => $this->getUserBranchId(), 'kpm_status' => 0]);
                }
            } else {
                return back()->with('error', 'Jumlah input stock tidak sesuai dengan range kode buku');
            }
        } else {
            $prefix = $this->generatePrefixCode((int)$input['product_id']);
            $code = $this->generateSavingBookCode($prefix, $input['product_id'], false);
            for ($i = (int)$code + 1; $i <= $input['count'] + (int)$code; $i++) {
                SavingBook::create(['code' => IdGenerator::generateFromDB($i, 6, $prefix, false), 'product_id' => $input['product_id'], 'branch_id' => $this->getUserBranchId(), 'kpm_status' => 0]);
            }
        }

        StockIn::create([
            'count' => $input['count'],
            'date_in' => $input['date_in'],
            'product_id' => $input['product_id'],
            'stock_id' => $this->getDynamicStockId($input['product_id'], $this->getUserBranchId()),
            'last_stock' => $this->getLastStock($this->getDynamicStockId($input['product_id'], $this->getUserBranchId()))
        ]);

        $this->updateStock($this->getDynamicStockId($input['product_id'], $this->getUserBranchId()), $input['count']);
        return back()->with('success', 'Stock Added Succesfully');
        // return response()->json($generatedCode);
    }

    function postBranchStock(Request $request)
    {
        // ambil inputan
        $input = $request->all();
        if ((int)$input['product_id'] == 5) {
            $codeLowest = $this->getCode($input['book_code_lowest'], true);
            $codeHighest = $this->getCode($input['book_code_highest'], true);
        } else {
            $codeLowest = $this->getCode($input['book_code_lowest']);
            $codeHighest = $this->getCode($input['book_code_highest']);
        }
        $generatedCode = [];

        // tracking product
        for ($i = (int)$codeLowest; $i <= (int)$codeHighest; $i++) {
            if ((int)$input['product_id'] == 5) {
                array_push($generatedCode, IdGenerator::generateFromDB($i, 6, $this->generatePrefixCode((int)$input['product_id']), true));
            } else {
                array_push($generatedCode, IdGenerator::generateFromDB($i, 6, $this->generatePrefixCode((int)$input['product_id'])));
            }
        }

        $checkStock = Stock::with('product')->where('product_id', $input['product_id'])->where('branch_id', $this->getUserBranchId())->first();

        if ($checkStock->current_stock < count($generatedCode)) {
            return back()->with('error', 'Stock ' . $checkStock->product->name . ' Tidak Cukup');
        } else {

            // update buku tabungan / bilyet
            $savingBook = SavingBook::whereIn('code', $generatedCode);
            $savingBook->update([
                'kpm_status' => 1,
                'branch_id' => $input['branch_id']
            ]);

            // tambah stock in cabang terkait
            StockIn::create([
                'count' => count($generatedCode),
                'date_in' => $input['date'],
                'product_id' => $input['product_id'],
                'stock_id' => $this->getDynamicStockId($input['product_id'], $input['branch_id']),
                'last_stock' => $this->getLastStock($this->getDynamicStockId($input['product_id'], $input['branch_id']))
            ]);

            // tambah stock cabang terkait
            $this->updateStock($this->getDynamicStockId($input['product_id'], $input['branch_id']), count($generatedCode));

            // kurangi jumlah stock KPM
            $this->decreaseStock($this->getDynamicStockId($input['product_id'], $this->getUserBranchId()), count($generatedCode));

            // tambah stock out kpm
            KpmStockOut::create([
                'count' =>  count($generatedCode),
                'date_out' => $input['date'],
                'branch_id' => $input['branch_id'],
                'stock_id' => $this->getDynamicStockId($input['product_id'], $input['branch_id'])
            ]);

            // return result
            return back()->with('success', 'Buku Tabungan Berhasil Dikirim Ke Cabang ' . Branch::find($input['branch_id'])->first()->name);
            // return response()->json($savingBook);
        }
    }


    // Helpers Function
    private function getCode($productCode, $isDeposit = false)
    {
        if ($isDeposit == false) {
            # code..
            $code = substr($productCode, 4, 6);
        } else {
            $code = ltrim($productCode, '0');
        }
        return $code;
    }

    private function getBranchTotalStock($branchId)
    {
        $checkStock = Stock::where('branch_id', $branchId)->get();
        if (count($checkStock) <= 0) {
            return "Belum Setup Stock";
        } else {
            $totalStocks = SavingBook::where('branch_id', $branchId)->where('soft_deletes', 1)->where('kpm_status', 1)->count();
            return $totalStocks;
        }
    }

    private function getDynamicStockId($productId, $branchId)
    {
        $stockId = Stock::query()->where('product_id', $productId)->where('branch_id', $branchId)->first();
        return $stockId->id;
    }

    private function getSavingBookByProduct()
    {
        $savingBook = SavingBook::where('branch_id', $this->getUserBranchId())->where('kpm_status', 1)->get();
        return $savingBook;
    }

    private function generatePrefixCode($productId)
    {
        $product = Product::where('id', $productId)->first();
        return $product->prefix == null ? "" : $product->prefix;
    }

    private function updateStock($id, $newStock)
    {
        $stock = Stock::find($id);
        $stock->update([
            'last_stock' => $stock->current_stock,
            'current_stock' => $stock->current_stock + $newStock
        ]);
    }

    private function decreaseStock($id, $newStock)
    {
        $stock = Stock::find($id);
        $stock->update([
            'last_stock' => $stock->current_stock,
            'current_stock' => $stock->current_stock - $newStock
        ]);
    }

    private function getLastStock($id)
    {
        $stock = Stock::find($id);
        return $stock->current_stock;
    }

    private function generateBookCode($prefix, $isDeposit = false)
    {
        switch ($prefix) {
            case 'TAB':
                $code = $isDeposit == false ? $prefix . "-000000" : "000000";
                break;
            case 'TAM':
                $code = $isDeposit == false ? $prefix . "-000000" : "000000";
                break;
            case 'TAU':
                $code = $isDeposit == false ? $prefix . "-000000" : "000000";
                break;
            case 'SIG':
                $code = $isDeposit == false ? $prefix . "-000000" : "000000";
                break;
            default:
                # code...
                break;
        }
        // $code = $isDeposit == false ? $prefix."-000000" : "000000";
        $code = substr($code, 4, 6);
        $code = ltrim($code, '0');
        return $code;
    }
}
