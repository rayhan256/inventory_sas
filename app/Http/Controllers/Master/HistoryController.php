<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\KpmStockOut;
use App\Models\StockIn;
use App\Models\StockOut;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    //
    function index(Request $request)
    {
        $stockins = StockIn::with(['stock.branch', 'stock.product'])->latest()->get();
        return view('master.history.index', ['histories' => $stockins]);
    }

    function stockOut()
    {
        $stockOuts = KpmStockOut::with('branch')->with(['stock.branch', 'stock.product'])->latest()->get();
        return view('master.history.stockout', ['histories' => $stockOuts]);
    }
}
