<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\User;
use App\Models\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserManagementController extends Controller
{
    //
    function index() {
        $users = User::with('detail.branch')->get();
        return view('master.users.index', ['users' => $users]);
    }

    function add() {
        return view('master.users.create');
    }

    function post(Request $request) {
        $input = $request->all();
        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
            'role' => $input['role']
        ]);

        return redirect(route('master-user.add-branch', $user->id));
    }

    function postDetail(Request $request) {
        $input = $request->all();
        UserDetail::create([
            'branch_id' => $input['branch_id'],
            'user_id' => $input['user_id']
        ]);
        return redirect(route('master-user.index'))->with('success', 'New Account Added');
    }

    function addBranch($id) {
        $branches = Branch::all();
        return view('master.users.create-detail', ['branches' => $branches, 'id' => $id]);
    }

    function editView($id) {
        $user = User::with('detail.branch')->where('id', $id)->first();
        return view('master.users.update', ['user' => $user, 'branches' => Branch::all()]);
    }

    function edit(Request $request) {
        $input = $request->all();
        $user = User::find($input['id']);
        $user->update([
            'name' => $input['name'],
            'email' => $input['email'],
            'role' => $input['role']
        ]);

        if ($request->has('branch_id')) {
           if (isset($user->detail)) {
                $detail = UserDetail::find($user->detail->id);
                $detail->update([
                    'branch_id' => $input['branch_id']
                ]);
           } else {
               UserDetail::create([
                   'user_id' => $user->id,
                   'branch_id' => $input['branch_id']
               ]);
           }
        }
        return redirect(route('master-user.index'))->with('success', 'Account Updated');
    }

    function destroy($id) {
        $user = User::find($id);
        $user->delete();

        return back()->with('success', 'Account Deleted');
    }
}
