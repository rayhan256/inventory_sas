<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Debtor;
use App\Models\Product;
use App\Models\SavingBook;


class MasterController extends Controller
{
    //
    function getAllProduct() {
        return response()->json(['data' => Product::all()]);
    }

    function getSavingBook() {
        return response()->json(['data' => SavingBook::all()]);
    }

    function getSavingBookByProduct($productId, $branchId) {
        $savingBook = SavingBook::where('product_id', $productId)->where('branch_id', $this->getUserBranchId());
        return response()->json(['data' => $savingBook]);
    }

    function getSavingBookBranch($product_id) {
        $savingBook = SavingBook::where('branch_id', $this->getUserBranchId())->where('product_id',  $product_id)->where('status', 0)->first();
        return response()->json($savingBook);
    }

    
    function getDebtorByNik($nik) {
        $debtor = Debtor::where('branch_id', $this->getUserBranchId())->where('nik', $nik)->first();
        return response()->json($debtor);
    }
}
