<?php

namespace App\Http\Controllers\Manops;

use App\Http\Controllers\Controller;
use App\Models\SavingBook;
use App\Models\Stock;
use App\Models\Submission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class SubmissionRequestController extends Controller
{
    //
    function index() {
        $data = [
            'submissions' => $this->getBrokenStockSubmission()
        ];
        return view('manops.stock_submission.index', $data);
    }

    function handleSavingBookSubmission($id, $savingBookId) {
        $savingBook = SavingBook::with(['product.stock'])->where('id', $savingBookId)->first();
        $submission = Submission::find($id);
        $savingBook->update([
            'status' => $this->generateStatusCode($submission->status)
        ]);
        $submission->update([
            'submission_status' => 0
        ]);
        $this->decreaseStock($savingBook->product->stock->id);
        return back()->with('success', 'Submission Approved');
    }

    function handleDestroySubmission($id) {
        $submission = Submission::find($id);
        $oldfile = public_path('uploads/submissions/'.$submission->file);
        if (File::exists($oldfile)) {
            File::delete($oldfile);
        }
        $submission->delete();
        return back()->with('success', 'Submisison Deleted');
    }    

    private function generateStatusCode($statusCode) {
        switch ($statusCode) {
            case 0:
                $statusCode = 3;
                break;
            case 1:
                $statusCode = 2;
                break;
            default:
                $statusCode = 3;
                break;
        }
        return $statusCode;
    }

    private function decreaseStock($id) {
        $stock = Stock::find($id);
        $stock->update([
            'last_stock' => $stock->current_stock,
            'current_stock' => $stock->current_stock - 1
        ]);
    }

    private function getBrokenStockSubmission() {
        $submission = Submission::with('savingBook')->get();
        return $submission;
    }
}
