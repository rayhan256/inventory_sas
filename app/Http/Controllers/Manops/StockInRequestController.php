<?php

namespace App\Http\Controllers\Manops;

use App\Http\Controllers\Controller;
use App\Models\Submission;
use Illuminate\Http\Request;

class StockInRequestController extends Controller
{
    //
    function index() {
        $data = [
            'submission' => $this->getBrokenStockSubmission()
        ];
        return view('manops.stock_submission.index', $data);
    }

    private function getBrokenStockSubmission() {
        $submission = Submission::with('savingBook')->get();
        return $submission;
    }
}
