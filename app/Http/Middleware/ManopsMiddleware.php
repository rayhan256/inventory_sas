<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ManopsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $role = Auth::user()->role;
        if ($role == 2) {
            return $next($request);
        }
        return redirect(route('auth.signout'))->with('error', 'Role Anda Tidak Diizinkan Untuk Memasuki Halaman Ini');
    }
}
