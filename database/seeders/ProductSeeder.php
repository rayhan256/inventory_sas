<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert([
            'name' => "TAMARA",
            'code' => "01",
        ]);
        DB::table('products')->insert([
            'name' => "TABUNGANKU",
            'code' => "02",
        ]);
        DB::table('products')->insert([
            'name' => "SIGELEGAR",
            'code' => "03",
        ]);
        DB::table('products')->insert([
            'name' => "TAUBAH",
            'code' => "04",
        ]);
        DB::table('products')->insert([
            'name' => "Bilyet Deposito",
            'code' => "05",
        ]);
    }
}
