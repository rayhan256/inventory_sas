<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // KPO
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 1,
            'branch_id' => 2,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 2,
            'branch_id' => 2,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 3,
            'branch_id' => 2,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 4,
            'branch_id' => 2,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 5,
            'branch_id' => 2,
        ]);

        // Pemalang
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 1,
            'branch_id' => 3,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 2,
            'branch_id' => 3,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 3,
            'branch_id' => 3,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 4,
            'branch_id' => 3,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 5,
            'branch_id' => 3,
        ]);

        // Brangsong
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 1,
            'branch_id' => 4,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 2,
            'branch_id' => 4,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 3,
            'branch_id' => 4,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 4,
            'branch_id' => 4,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 5,
            'branch_id' => 4,
        ]);

        // Mranggen
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 1,
            'branch_id' => 5,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 2,
            'branch_id' => 5,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 3,
            'branch_id' => 5,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 4,
            'branch_id' => 5,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 5,
            'branch_id' => 5,
        ]);

        // Ambarawa
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 1,
            'branch_id' => 6,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 2,
            'branch_id' => 6,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 3,
            'branch_id' => 6,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 4,
            'branch_id' => 6,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 5,
            'branch_id' => 6,
        ]);

        // Semarang
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 1,
            'branch_id' => 7,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 2,
            'branch_id' => 7,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 3,
            'branch_id' => 7,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 4,
            'branch_id' => 7,
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 5,
            'branch_id' => 7,
        ]);

    }
}
