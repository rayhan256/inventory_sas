<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KpmStockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 1,
            'branch_id' => 1
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 2,
            'branch_id' => 1
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 3,
            'branch_id' => 1
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 4,
            'branch_id' => 1
        ]);
        DB::table('stocks')->insert([
            'last_stock' => 0,
            'current_stock' => 0,
            'product_id' => 5,
            'branch_id' => 1
        ]);
    }
}
