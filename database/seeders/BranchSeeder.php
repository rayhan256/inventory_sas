<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('branches')->insert([
            'name' => "KPM",
            'code' => "00",
        ]);
        DB::table('branches')->insert([
            'name' => "KPO",
            'code' => "01",
        ]);
        DB::table('branches')->insert([
            'name' => "Pemalang",
            'code' => "02",
        ]);
        DB::table('branches')->insert([
            'name' => "Brangsong",
            'code' => "03",
        ]);
        DB::table('branches')->insert([
            'name' => "Mranggen",
            'code' => "04",
        ]);
        DB::table('branches')->insert([
            'name' => "Ambarawa",
            'code' => "05",
        ]);
        DB::table('branches')->insert([
            'name' => "Semarang",
            'code' => "06",
        ]);
    }
}
