<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submissions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('saving_book_id');

            $table->date('submission_date');
            $table->string('desc');
            $table->string('file');
            $table->integer('status'); // 0 => rusak, 1 => hilang
            $table->boolean('submission_status'); // 0 => acc, 1 => decline

            $table->foreign('saving_book_id')->references('id')->on('saving_books')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('submissions');
    }
}
