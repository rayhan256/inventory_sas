<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDebtorColumnsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('debtors', function (Blueprint $table) {
            //
            $table->string('city')->nullable()->change();
            $table->string('province')->nullable()->change();
            $table->string('kec')->nullable()->change();
            $table->string('kel')->nullable()->change();
            $table->string('email')->nullable()->change();
            $table->string('postal_code')->nullable()->change();
            $table->string('phone')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('debtors', function (Blueprint $table) {
            //
        });
    }
}
