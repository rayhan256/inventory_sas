<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBilyetOutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bilyet_outs', function (Blueprint $table) {
            $table->id();
            $table->integer('count');
            $table->unsignedBigInteger('bilyet_deposit_id');
            $table->unsignedBigInteger('debtor_id');
            $table->unsignedBigInteger('stock_id');

            $table->foreign('bilyet_deposit_id')->references('id')->on('bilyet_deposits')->onDelete('CASCADE');
            $table->foreign('debtor_id')->references('id')->on('debtors')->onDelete('CASCADE');
            $table->foreign('stock_id')->references('id')->on('stocks')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bilyet_outs');
    }
}
