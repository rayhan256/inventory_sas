<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeaderOboxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('header_oboxes', function (Blueprint $table) {
            $table->id();
            $table->string('flag_header')->default('H');
            $table->string('ljk_type')->default('01020101');
            $table->string('ljk_code');
            $table->string('datatype')->default("A");
            $table->date('period');
            $table->integer('data_count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('header_oboxes');
    }
}
