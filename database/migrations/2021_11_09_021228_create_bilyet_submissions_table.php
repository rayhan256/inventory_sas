<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBilyetSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bilyet_submissions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('bilyet_deposit_id');

            $table->date('submission_date');
            $table->text('desc');
            $table->text('file');
            $table->boolean('status');
            $table->integer('submission_status');

            $table->foreign('bilyet_deposit_id')->references('id')->on('bilyet_deposits')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bilyet_submissions');
    }
}
