<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockOutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_outs', function (Blueprint $table) {
            $table->id();
            $table->integer('count');
            $table->unsignedBigInteger('saving_book_id');

            $table->unsignedBigInteger('debtor_id');

            $table->foreign('saving_book_id')->references('id')->on('saving_books')->onDelete('CASCADE');
            $table->foreign('debtor_id')->references('id')->on('debtors')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_outs');
    }
}
