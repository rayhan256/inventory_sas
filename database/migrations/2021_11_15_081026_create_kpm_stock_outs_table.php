<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKpmStockOutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kpm_stock_outs', function (Blueprint $table) {
            $table->id();
            $table->integer('count');
            $table->date('date_out');
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('stock_id');

            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('CASCADE');
            $table->foreign('stock_id')->references('id')->on('stocks')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kpm_stock_outs');
    }
}
