@extends('errors::illustrated-layout')
@section('title', __('Not Found'))
@section('code', '404')
@section('message', __('Halaman Tidak Ditemukan Mainnya Kejauhan, Yuk Pulang Ke Home Page'))
@section('image')
    <img src="{{asset('assets/errors/404.svg')}}" alt="">
@endsection
