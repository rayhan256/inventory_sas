@extends('errors::illustrated-layout')
@section('title', __('Server Error'))
@section('code', '500')
@section('message', __('Waduh ada error 😔. tenang.. silahkan hubungi KPM, nanti akan kita bantu kok.. '))
@section('image')
    <img src="{{asset('assets/errors/500.svg')}}" alt="">
@endsection
