@extends('users/templates/index', ['title' => 'Tambah Nasabah'])
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card bg-white shadow">
            <div class="card-header">
                <h4 class="card-title">Form Input</h4>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <form action="{{route('operator-debtors.post')}}" method="post" class="form form-horizontal">
                        @csrf
                        <div class="form-body">
                            <div class="my-3">
                                <label for="">No Rekening</label>
                                <input type="text" name="account_id" class="form-control" required>
                            </div>
                            <div class="my-3">
                                <label for="">Nama Lengkap</label>
                                <input type="text" name="name" class="form-control" required>
                            </div>
                            <div class="my-3">
                                <label for="">Alamat</label>
                                <input type="text" name="address" class="form-control" required>
                            </div>
                            <div class="my-3">
                                <label for="">NIK</label>
                                <input type="number" minlength="16" maxlength="16" name="nik" class="form-control" required>
                            </div>
                            <input type="hidden" name="branch_id" value="{{$branch_id}}" id="">
                            <input type="submit" value="Simpan" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    var return_first = function () {
        var tmp = null;
        $.ajax({
            'async': false,
            'type': "get",
            'global': false,
            'dataType': 'json',
            'url': 'https://x.rajaapi.com/poe',
            'success': function (data) {
                tmp = data.token;
            }
        });
        return tmp;
    }();
    $(document).ready(function () {
        $.ajax({
            url: 'https://dev.farizdotid.com/api/daerahindonesia/provinsi',
            type: 'GET',
            dataType: 'json',
            success: function (json) {
                if (json.provinsi.length > 0) {
                    for (i = 0; i < Object.keys(json.provinsi).length; i++) {
                        $('#provinsi').append($('<option>').text(json.provinsi[i].nama).attr('value',
                            json.provinsi[i].id));
                    }
                } else {
                    $('#kabupaten').append($('<option>').text('Data tidak di temukan').attr('value',
                        'Data tidak di temukan'));
                }
            }
        });
        $("#provinsi").change(function () {
            var propinsi = $("#provinsi").val();
            $.ajax({
                url: 'https://dev.farizdotid.com/api/daerahindonesia/kota',
                data: "id_provinsi=" + propinsi,
                type: 'GET',
                cache: false,
                dataType: 'json',
                success: function (json) {
                    $("#kabupaten").html('');
                    if (json.kota_kabupaten.length > 0) {
                        for (i = 0; i < Object.keys(json.kota_kabupaten).length; i++) {
                            $('#kabupaten').append($('<option>').text(json.kota_kabupaten[i].nama)
                                .attr('value', json.kota_kabupaten[i].id));
                        }
                        $('#kecamatan').html($('<option>').text('-- Pilih Kecamatan --')
                            .attr('value', '-- Pilih Kecamatan --'));
                        $('#kelurahan').html($('<option>').text('-- Pilih Kelurahan --')
                            .attr('value', '-- Pilih Kelurahan --'));

                    } else {
                        $('#kabupaten').append($('<option>').text('Data tidak di temukan')
                            .attr('value', 'Data tidak di temukan'));
                    }
                }
            });
        });
        $("#kabupaten").change(function () {
            var kabupaten = $("#kabupaten").val();
            var propinsi = $("#provinsi").val()
            $.ajax({
                url: 'https://dev.farizdotid.com/api/daerahindonesia/kecamatan',
                data: "id_kota=" + kabupaten,
                type: 'GET',
                cache: false,
                dataType: 'json',
                success: function (json) {
                    $("#kecamatan").html('');
                    console.log(json.kecamatan);
                    if (json.kecamatan.length > 0) {
                        for (i = 0; i < Object.keys(json.kecamatan).length; i++) {
                            $('#kecamatan').append($('<option>').text(json.kecamatan[i].nama)
                                .attr('value', json.kecamatan[i].id));
                        }
                        $('#kelurahan').html($('<option>').text('-- Pilih Kelurahan --')
                            .attr('value', '-- Pilih Kelurahan --'));

                    } else {
                        $('#kecamatan').append($('<option>').text('Data tidak di temukan')
                            .attr('value', 'Data tidak di temukan'));
                    }
                }
            });
        });
        $("#kecamatan").change(function () {
            var kabupaten = $("#kabupaten").val();
            var propinsi = $("#provinsi").val()
            var kecamatan = $("#kecamatan").val();
            $.ajax({
                url: 'https://dev.farizdotid.com/api/daerahindonesia/kelurahan',
                data: "id_kecamatan=" + kecamatan,
                type: 'GET',
                dataType: 'json',
                cache: false,
                success: function (json) {
                    $("#kelurahan").html('');
                    if (json.kelurahan.length > 0) {
                        for (i = 0; i < Object.keys(json.kelurahan).length; i++) {
                            $('#kelurahan').append($('<option>').text(json.kelurahan[i].nama)
                                .attr('value', json.kelurahan[i].id));
                        }
                    } else {
                        $('#kelurahan').append($('<option>').text('Data tidak di temukan')
                            .attr('value', 'Data tidak di temukan'));
                    }
                }
            });
        });
    });

</script>
@endsection
