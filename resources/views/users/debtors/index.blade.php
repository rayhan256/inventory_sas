@extends('users/templates/index', ['title' => 'Data Nasabah'])
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card bg-white shadow">
            <div class="card-header">
                <a href="{{route('operator-debtors.add')}}" class="btn btn-primary">Tambah</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No Rekening</th>
                                <th>Nama</th>
                                <th>NIK</th>
                                {{-- <th>Aksi</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $debtor)
                            <tr>
                                <td>{{$debtor->account_id}}</td>
                                <td>{{$debtor->name}}</td>
                                <td>{{$debtor->nik}}</td>
                                {{-- <td>
                                    <a href="{{route('operator-debtors.edit', $debtor->id)}}" class="btn btn-primary">Ubah</a>
                                    <a href="{{route('operator-debtors.destroy', $debtor->id)}}}}" class="btn btn-danger" onclick="return confirm('Anda Yakin Ingin Mengapus Nasabah Ini ?')">Hapus</a>
                                </td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
 <script>
     $('.table').DataTable()
 </script>
@endsection
