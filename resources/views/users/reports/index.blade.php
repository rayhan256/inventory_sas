@extends('users/templates/index', ['title' => 'Laporan'])
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header my-3">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <a class="btn btn-success" href="{{route('operator-report.export')}}">Export Excel</a>
                        <a href="{{route('operator-report.export-pdf')}}" class="btn btn-primary">Download Berita Acara</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>No Rekening</th>
                                <th>Alamat</th>
                                <th>NIK</th>
                                <th>No Buku</th>
                                <th>Tanggal Keluar</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $index = 1;
                            @endphp
                            @foreach ($reports as $report)
                            @foreach ($report->stockOut as $item)
                            <tr>
                                <td>{{$index++}}</td>
                                <td>{{$item->debtor->name}}</td>
                                <td>{{$item->debtor->account_id}}</td>
                                <td>{{$item->debtor->address}}</td>
                                <td>{{$item->debtor->nik}}</td>
                                <td>{{$item->savingBook->code ?? ""}}</td>
                                <td>{{date('d-m-Y', strtotime($item->created_at))}}</td>
                                <td>{{$item->desc ?? '-'}}</td>
                            </tr>
                            @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $('.table').dataTable()

</script>
@endsection
