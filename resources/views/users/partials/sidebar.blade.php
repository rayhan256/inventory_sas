<div id="sidebar" class="active">
    <div class="sidebar-wrapper active">
        <div class="sidebar-header">
            <div class="d-flex justify-content-between">
                <div class="logo">
                    <a href="/" style="font-size: 18px;">SAS Inventory - Operator</a>
                </div>
                <div class="toggler">
                    <a href="/" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                </div>
            </div>
        </div>
        <div class="sidebar-menu">
            <ul class="menu">
                <li class="sidebar-item ">
                    <a href="/" class='sidebar-link'>
                        <i class="bi bi-grid-fill"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="sidebar-title">Menu</li>
                <li class="sidebar-item  has-sub">
                    <a href="#" class='sidebar-link'>
                        <i class="bi bi-stack"></i>
                        <span>Data Master</span>
                    </a>
                    <ul class="submenu ">
                        <li class="submenu-item ">
                            <a href="{{route('operator-debtors.index')}}">Nasabah</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="{{route('operator-saving-books.index')}}">Stok Buku</a>
                        </li>
                    </ul>
                </li>

                <li class="sidebar-title">Manajemen Stok</li>
                <li class="sidebar-item  ">
                    {{-- <a href="{{route('operator-stock.generate', 1)}}" class='sidebar-link'>
                        <i class="bi bi-file-zip"></i>
                        <span>Setup Stok</span>
                    </a> --}}
                    <a href="{{route('operator-stock.index')}}" class='sidebar-link'>
                        <i class="bi bi-file-zip-fill"></i>
                        <span>Kontrol Stok</span>
                    </a>
                </li>
                <li class="sidebar-title">Laporan</li>

                <li class="sidebar-item  ">
                    <a href="{{route('operator-report.index')}}" class='sidebar-link'>
                        <i class="bi bi-grid-1x2-fill"></i>
                        <span>Laporan Umum</span>
                    </a>
                </li>
                <li class="sidebar-title">Pengajuan</li>
                <li class="sidebar-item  ">
                    <a href="{{route('operator-submission.index')}}" class='sidebar-link'>
                        <i class="bi bi-grid-1x2-fill"></i>
                        <span>Stock Hilang / Rusak</span>
                    </a>
                </li>
                <li class="sidebar-title">Pengaturan</li>
                <li class="sidebar-item ">
                    <a href="{{route('auth.signout')}}" onclick="return confirm('anda yakin ingin keluar ? ')" class='sidebar-link'>
                        <i class="bi bi-door-closed-fill"></i>
                        <span class="text-danger">Logout</span>
                    </a>
                </li>
            </ul>
        </div>
        <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
    </div>
</div>
