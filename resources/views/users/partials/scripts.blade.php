<script src="{{asset('assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>

<script src="{{asset('assets/vendors/apexcharts/apexcharts.js')}}"></script>
<script src="{{asset('assets/js/pages/dashboard.js')}}"></script>

<script src="{{asset('assets/js/mazer.js')}}"></script>
<script src="{{asset('assets/vendors/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendors/jquery-datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/vendors/jquery-datatables/custom.jquery.dataTables.bootstrap5.min.js')}}"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.14.0-beta2/dist/js/bootstrap-select.min.js"></script>
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase.js"></script>
<script>
     const firebaseConfig = {
      apiKey: "AIzaSyDSOZ69abhtwYDPWaTRpWCahORwY4T9jCA",
      authDomain: "inventory-sas.firebaseapp.com",
      projectId: "inventory-sas",
      storageBucket: "inventory-sas.appspot.com",
      messagingSenderId: "400732378158",
      appId: "1:400732378158:web:1ad5425d282f65e0e1bbbf",
      measurementId: "G-SSSFPL5XWV"
    };
    firebase.initializeApp(firebaseConfig);
    const messaging = firebase.messaging();
</script>