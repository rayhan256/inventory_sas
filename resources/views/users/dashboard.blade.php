@extends('users/templates/index', ['title' => 'Dashboard Cabang '.$branch_name])
@section('content')
<section class="row">
    <div class="col-12">
        <div class="row">
           @if (count($stocks) <= 0 || $stocks == null)
			   <div class="row">
					<div class="col-12">
						<div class="alert alert-danger">Belum Setup Stock |<a href="{{route('operator-stock.generate')}}" class="text-primary"> Setup Disini</a></div>
					</div>
			   </div>
		   @else
		   @foreach ($stocks as $stock)
		   <div class="col-6 col-lg-3 col-md-6">
			   <div class="card">
				   <div class="card-body px-3 py-4-5">
					   <div class="row">
						   <div class="col-md-4">
							   <div class="stats-icon purple">
								   <i class="iconly-boldShow"></i>
							   </div>
						   </div>
						   <div class="col-md-8">
							   <h6 class="text-muted font-semibold">Stock {{$stock->product->name}}</h6>
							   <h6 class="font-extrabold mb-0">{{$stock->current_stock}}</h6>
						   </div>
					   </div>
				   </div>
			   </div>
		   </div>
		   @endforeach
		   @endif
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Grafik Jumlah Stock</h4>
                    </div>
                    <div class="card-body">
                        <div id="chart-profile-visit"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
	let dataStock = []
	let dataLabel = []
</script>
@foreach ($stocks as $stock)
	<script>
		dataLabel.push('{{$stock->product->name}}')
	</script>
@endforeach
@foreach ($stocks as $stock)
	<script>
		dataStock.push(parseInt('{{$stock->current_stock}}'))
	</script>
@endforeach
<script>
    var optionsProfileVisit = {
	annotations: {
		position: 'back'
	},
	dataLabels: {
		enabled:false
	},
	chart: {
		type: 'bar',
		height: 300
	},
	fill: {
		opacity:1
	},
	plotOptions: {
	},
	series: [{
		name: 'sales',
		data: dataStock
	}],
	colors: '#435ebe',
	xaxis: {
		categories: dataLabel,
	},
}
let optionsVisitorsProfile  = {
	series: [70, 30],
	labels: ['Male', 'Female'],
	colors: ['#435ebe','#55c6e8'],
	chart: {
		type: 'donut',
		width: '100%',
		height:'350px'
	},
	legend: {
		position: 'bottom'
	},
	plotOptions: {
		pie: {
			donut: {
				size: '30%'
			}
		}
	}
}

var optionsEurope = {
	series: [{
		name: 'series1',
		data: [310, 800, 600, 430, 540, 340, 605, 805,430, 540, 340, 605]
	}],
	chart: {
		height: 80,
		type: 'area',
		toolbar: {
			show:false,
		},
	},
	colors: ['#5350e9'],
	stroke: {
		width: 2,
	},
	grid: {
		show:false,
	},
	dataLabels: {
		enabled: false
	},
	xaxis: {
		type: 'datetime',
		categories: ["2018-09-19T00:00:00.000Z", "2018-09-19T01:30:00.000Z", "2018-09-19T02:30:00.000Z", "2018-09-19T03:30:00.000Z", "2018-09-19T04:30:00.000Z", "2018-09-19T05:30:00.000Z", "2018-09-19T06:30:00.000Z","2018-09-19T07:30:00.000Z","2018-09-19T08:30:00.000Z","2018-09-19T09:30:00.000Z","2018-09-19T10:30:00.000Z","2018-09-19T11:30:00.000Z"],
		axisBorder: {
			show:false
		},
		axisTicks: {
			show:false
		},
		labels: {
			show:false,
		}
	},
	show:false,
	yaxis: {
		labels: {
			show:false,
		},
	},
	tooltip: {
		x: {
			format: 'dd/MM/yy HH:mm'
		},
	},
};

let optionsAmerica = {
	...optionsEurope,
	colors: ['#008b75'],
}
let optionsIndonesia = {
	...optionsEurope,
	colors: ['#dc3545'],
}



var chartProfileVisit = new ApexCharts(document.querySelector("#chart-profile-visit"), optionsProfileVisit);
var chartVisitorsProfile = new ApexCharts(document.getElementById('chart-visitors-profile'), optionsVisitorsProfile)
var chartEurope = new ApexCharts(document.querySelector("#chart-europe"), optionsEurope);
var chartAmerica = new ApexCharts(document.querySelector("#chart-america"), optionsAmerica);
var chartIndonesia = new ApexCharts(document.querySelector("#chart-indonesia"), optionsIndonesia);

chartIndonesia.render();
chartAmerica.render();
chartEurope.render();
chartProfileVisit.render();
chartVisitorsProfile.render()
</script>
@endsection
