<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
 * {
        font-family: sans-serif;
        box-sizing: border-box;
    }

@page {
  size: A4;
  margin: 0;
}

.page {
  width: 21cm;
  min-height: 29.7cm;
  padding: 2cm;
  margin: 1cm auto;
  border: 1px #D3D3D3 solid;
  border-radius: 5px;
  background: white;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}

@media print {
  .page {
    margin: 0;
    border: initial;
    border-radius: initial;
    width: initial;
    min-height: initial;
    box-shadow: initial;
    background: initial;
    page-break-after: always;
  }
}
    

    .page .header-title {
        width: 100%;
        font-size: 13px;
        font-weight: bold;
        text-align: center;
    }

    .page .header-content {
        text-align: justify;
        margin-top: 15px;
        font-size: 12px;
    }

    .table-container {
        width: 100%;
        text-align: center;
    }

    .table-container .table-title {
        font-weight: bold;
        font-size: 11px;
        margin: 10px 0;
        text-align: start;
    }

    table, th, td {
        border: 1px solid black;
        /* margin: 0 auto; */
        font-size: 12px;
    }

    th {
        width: 200px;
    }

    td {
        text-align: start;
        padding: 5px 5px;
    }

    .value {
        text-align: center;
    }

    .text-content {
        font-size: 12px;
        padding: 35px 0;
    }
</style>
<body contenteditable="true">
    <div class="page">
        <div class="header-title">
            BERITA ACARA PEMERIKSAAN STOK BUKU TABUNGAN DAN BILYET DEPOSITO
        </div>
        <div class="header-content">
            Pada tanggal {{$date}} telah dilakukan pemeriksaan stok Buku Tabungan dan Bilyet 
            Deposito untuk periode ......... sampai dengan .......... oleh Costumer Service 
            dan diperiksa oleh Kepala Bagian Operasional dengan rincian sebagai berikut :
        </div>

        <div class="table-container">
            <div class="table-title">Buku Tabungan</div>
            <table>
                <thead>
                    <tr>
                        <th>Periode</th>
                        <th>........... s/d ...........</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Stok Awal</td>
                        <td class="value">{{$last_stock}}</td>
                    </tr>
                    <tr>
                        <td>Stok Tambah</td>
                        <td class="value">{{$stock_in}}</td>
                    </tr>
                    <tr>
                        <td>Stok Pakai</td>
                        <td class="value">{{$stock_out}}</td>
                    </tr>
                    <tr>
                        <td>Stok Rusak</td>
                        <td class="value">{{$broken_stock}}</td>
                    </tr>
                    <tr>
                        <td>Stok Hilang</td>
                        <td class="value">{{$lost_stock}}</td>
                    </tr>
                    <tr>
                        <td>Stok Akhir</td>
                        <td class="value">{{$current_stock}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table-container">
            <div class="table-title">Bilyet Deposito</div>
            <table>
                <thead>
                    <tr>
                        <th>Periode</th>
                        <th>........... s/d ...........</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Stok Awal</td>
                        <td class="value">{{$last_stock_bilyet}}</td>
                    </tr>
                    <tr>
                        <td>Stok Tambah</td>
                        <td class="value">{{$stock_in_bilyet}}</td>
                    </tr>
                    <tr>
                        <td>Stok Pakai</td>
                        <td class="value">{{$stock_out_bilyet}}</td>
                    </tr>
                    <tr>
                        <td>Stok Rusak</td>
                        <td class="value">{{$broken_stock_bilyet}}</td>
                    </tr>
                    <tr>
                        <td>Stok Hilang</td>
                        <td class="value">{{$lost_stock_bilyet}}</td>
                    </tr>
                    <tr>
                        <td>Stok Akhir</td>
                        <td class="value">{{$current_stock_bilyet}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table-container">
            <div class="table-title">Dengan Rincian</div>
            <table>
                <thead>
                    <tr>
                        <th>Periode</th>
                        <th>No Seri</th>
                        <th>Jumlah</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Bilyet Deposito</td>
                        <td class="value">{{$bilyet_l}} - {{$bilyet_h}}</td>
                        <td class="value">{{$last_stock_bilyet}}</td>
                    </tr>
                    <tr>
                        <td>Buku Tabungan Tamara</td>
                        <td class="value">{{$tamara_l}} - {{$tamara_h}}</td>
                        <td class="value">0</td>
                    </tr>
                    <tr>
                        <td>Buku Tabunganku</td>
                        <td class="value">{{$tabunganku_l}} - {{$tabunganku_h}}</td>
                        <td class="value">0</td>
                    </tr>
                    <tr>
                        <td>Buku Tabungan Sigelegar</td>
                        <td class="value">{{$gelegar_l}} - {{$gelegar_h}}</td>
                        <td class="value">0</td>
                    </tr>
                    <tr>
                        <td>Buku Tabungan Taubah</td>
                        <td class="value">{{$taubah_l}} - {{$taubah_h}}</td>
                        <td class="value">0</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="text-content">Demikian berita acara ini kami buat, untuk dapat dipergunakan sebagaimana mestinya</div>
        <div class="text-content location">
            .........., {{$date}}
        </div>
       <div>
        <div class="text-content" style="display: inline-block;width: 70%;">
            Customer Service
        </div>
        <div class="text-content" style="display: inline-block;">
            <div class="text-content">Pemeriksa, </div>
            <div class="text-content">Kabag Operasional </div>
        </div>
       </div>
    </div>
</body>
</html>