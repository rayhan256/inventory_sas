@extends('users/templates/index', ['title' => "Pengajuan Stock Buku Rusak / Hilang"])
@section('content')
    <div class="row">
        @if (Session::has('success'))
        <div class="col-12">
            <div class="alert alert-success">{{Session::get('success')}}</div>
        </div>
        @endif
        @if (count($errors) > 0)
            <div class="col-12">
                <div class="alert alert-danger">
                  @foreach ($errors->all() as $error)
                    {{ $error }}
                  @endforeach
                </div>
            </div>
          @endif
        <div class="col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h4>Form Pengajuan Stock Buku Rusak / Hilang</h4>
                    {{-- <a href="{{route('operator-submission-bilyet.index')}}" class="btn btn-primary">Switch To Bilyet</a> --}}
                </div>
                <div class="card-body">
                    <form action="{{route('operator-submission.post')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="my-3">
                            <label for="">Kode Buku</label>
                            <input type="text" name="saving_book_id" id="saving_book_code" class="form-control" required>
                        </div>
                        <div class="my-3">
                            <label for="">Tanggal Pengajuan</label>
                            <input type="date" name="submission_date" id="" class="form-control" required>
                        </div>
                        <div class="my-3">
                            <label for="">Deskripsi</label>
                            <textarea name="desc" id="" cols="30" rows="10" class="form-control" required></textarea>
                        </div>
                        <div class="my-3 row">
                           <div class="col-6">
                            <label for="file">Lampiran</label>
                            <input type="file" name="file" id="file" class="form-control" required>
                           </div>
                           <div class="col-6">
                                <label for="">Status</label>
                                <select name="status" id="" class="form-control" required>
                                    <option value="0">Rusak</option>
                                    <option value="1">Hilang</option>
                                </select>
                           </div>
                        </div>
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        let data = [];
    </script>
    @foreach ($saving_books as $book)
        <script>
            data.push('{{$book->code}}')
        </script>
    @endforeach
    <script>
        $(function() {
            $('#saving_book_code').autocomplete({
                source:  function (request, response) {
                    var results = $.ui.autocomplete.filter(data, request.term);
                    response(results.slice(0, 10));
                }
            })
        })
    </script>
@endsection