@extends('users/templates/index', ['title' => "Pengajuan Bilyet Rusak / Hilang"])
@section('content')
    <div class="row">
        @if (Session::has('success'))
        <div class="col-12">
            <div class="alert alert-success">{{Session::get('success')}}</div>
        </div>
        @endif
        <div class="col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h4>Form Pengajuan Stock Rusak / Hilang</h4>
                    <a href="{{route('operator-submission.index')}}" class="btn btn-primary">Switch To Butab</a>
                </div>
                <div class="card-body">
                    <form action="{{route('operator-submission-bilyet.post')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="my-3">
                            <label for="">Kode Bilyet</label>
                            <input type="text" name="bilyet_deposit_id" id="bilyet_code" class="form-control">
                        </div>
                        <div class="my-3">
                            <label for="">Tanggal Pengajuan</label>
                            <input type="date" name="submission_date" id="" class="form-control">
                        </div>
                        <div class="my-3">
                            <label for="">Deskripsi</label>
                            <textarea name="desc" id="" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                        <div class="my-3 row">
                           <div class="col-6">
                            <label for="file">Lampiran</label>
                            <input type="file" name="file" id="file" class="form-control">
                           </div>
                           <div class="col-6">
                                <label for="">Status</label>
                                <select name="status" id="" class="form-control">
                                    <option value="0">Rusak</option>
                                    <option value="1">Hilang</option>
                                </select>
                           </div>
                        </div>
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        let data = [];
    </script>
    @foreach ($bilyets as $book)
        <script>
            data.push('{{$book->code}}')
        </script>
    @endforeach
    <script>
        $(function() {
            $('#bilyet_code').autocomplete({
                source: data
            })
        })
    </script>
@endsection