@extends('users/templates/index', ['title' => 'Kontrol Stock'])
@section('content')
    <div class="row">
        <div class="col-12">
            @if (Session::has('success'))
                <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif
            @if (count($stocks) <= 0)
                <div class="row">
                    <div class="col-12">
                        <div class="alert alert-danger">Belum Setup Stock |<a href="{{ route('operator-stock.generate') }}"
                                class="text-primary"> Setup Disini</a></div>
                    </div>
                </div>
            @else
                <div class="row">
                    @foreach ($stocks as $stock)
                        <div class="col-6 col-lg-3 col-md-6">
                            <div class="card">
                                <div class="card-body px-3 py-4-5">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="stats-icon purple">
                                                <i class="iconly-boldShow"></i>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <h6 class="text-muted font-semibold"> {{ $stock->product->name }}</h6>
                                            <h6 class="font-extrabold mb-0">Stok Awal {{ $stock->last_stock }}
                                            </h6>
                                            <h6 class="font-extrabold mb-0">Stok Akhir {{ $stock->current_stock }}
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
            <div class="row">
                <div class="col-12">
                    <div class="card bg-white">
                        <div class="card-header">
                            <h4 class="card-title">Input Buku Tabungan Keluar</h4>

                        </div>

                        <div class="card-body">
                            <div class="row">
                                {{-- <div class="col-12">
                                    <form action="{{ route('operator-stock.outPost') }}" id="form1" method="post">
                                        @csrf

                                        <div class="my-3">
                                            <label for="">Jenis Buku</label>
                                            <select name="product_id" class="form-control" id="product">
                                                <option value="">Pilih Jenis Buku</option>
                                                @foreach ($products as $product)
                                                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="my-3">
                                            <label for="">Debitur</label>
                                            <select name="debtor_id" id="debtor_id" class="form-control">
                                                @foreach ($debtors as $debtor)
                                                    <option value="{{ $debtor->nik }}">{{ $debtor->nik }} -
                                                        {{ $debtor->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="my-3">
                                            <label for="">No Rekening</label>
                                            <select name="account_id" id="" class="form-control">
                                                @foreach ($debtors as $debtor)
                                                    <option value="{{ $debtor->account_id }}">{{ $debtor->account_id }} -
                                                        {{ $debtor->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="my-3">
                                            <label for="">Keterangan (Diisi Jika Ada Keterangan Tertentu)</label>
                                            <input type="text" name="desc" class="form-control">
                                        </div>

                                        <br>
                                        <div class="text-center">Kode Otomatis</div>
                                        <hr>
                                        <div class="my-3">
                                            <label for="">Kode Buku</label>
                                            <input type="text" class="form-control" name="saving_book_id" id="book"
                                                readonly>
                                        </div>
                                        <div class="my-3">
                                            <label for="">Nama Debitur</label>
                                            <input type="text" class="form-control" name="debtor_name" id="debtor_name"
                                                readonly>
                                        </div>

                                        <input type="submit" value="Submit" id="submit1" class="btn btn-primary">
                                    </form>
                                </div> --}}
                                {{-- manual input --}}
                                <div class="col-12">
                                    <form action="{{ route('operator-stock.outPostManual') }}" method="post"
                                        id="form2">
                                        @csrf
                                        <div class="mb-3">
                                            <label for="">Kode Buku</label>
                                            <ul>
                                                @foreach ($products as $product)
                                                    <li>{{ $product->prefix == null ? '02xxxxxx' : $product->prefix }}
                                                        : {{ $product->name }}</li>
                                                @endforeach
                                            </ul>
                                            <input type="text" name="saving_book_id" id="saving_book_id"
                                                class="form-control" required>
                                        </div>
                                        <div class="mb-3">
                                            <label for="">NIK Nasabah</label>
                                            <span class="text-danger d-none" id="error_nik">mohon isi format NIK dengan
                                                benar</span>
                                            <input type="text" name="debtor_id" id="debtor_id2" class="form-control"
                                                required>
                                        </div>
                                        <div class="mb-3">
                                            <label for="">Keterangan (Diisi Jika Ada Keterangan Tertentu)</label>
                                            <input type="text" name="desc" class="form-control">
                                        </div>
                                        <input type="submit" value="Submit" id="submit"
                                            class="btn btn-primary disabled">
                                    </form>
                                </div>
                            </div>
                        </div>
                        {{-- master.branchSavingBooks --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        let data = [];
        let debtor = []
    </script>
    @foreach ($saving_books as $book)
        <script>
            data.push('{{ $book->code }}')
        </script>
    @endforeach
    @foreach ($debtors as $debtor)
        <script>
            debtor.push('{{ $debtor->nik }} - {{ $debtor->account_id }} - {{ $debtor->name }}')
        </script>
    @endforeach
    <script>
        const bookInput = document.getElementById('book')
        const product = document.getElementById('product')
        const debtorId = document.getElementById('debtor_id')
        const debtorName = document.getElementById('debtor_name');
        const savingBook = document.getElementById('saving_book_id')

        document.getElementById('debtor_id2').addEventListener('change', (e) => {
            let value = document.getElementById('debtor_id2').value
            let splitValue = value.split(' - ')
            if (splitValue.length <= 1) {
                document.getElementById('error_nik').classList.remove('d-none')
            } else {
                document.getElementById('error_nik').classList.add('d-none')
                document.getElementById('submit').classList.remove('disabled')
            }
        })

        $('#saving_book_id').autocomplete({
            source: function(request, response) {
                var results = $.ui.autocomplete.filter(data, request.term);
                response(results.slice(0, 10));
            }
        })

        $('#debtor_id2').autocomplete({
            source: function(request, response) {
                var results = $.ui.autocomplete.filter(debtor, request.term);
                response(results.slice(0, 10));
            }
        })

        product.addEventListener('change', () => {
            fetch(`https://bprsas.id/books/${product.value}`)
                .then(response => response.json())
                .then(data => bookInput.value = data.code)
        })

        debtorId.addEventListener('change', () => {
            fetch(`https://bprsas.id/debtors/${debtorId.value}`)
                .then(response => response.json())
                .then(data => debtorName.value = data.name)
        })

        document.querySelector('#form1').addEventListener('submit', (e) => {

            if (bookInput.value.length <= 0 || debtorName.value.length <= 0) {
                e.preventDefault()
                console.log(product.value);
                alert('kode buku atau nama debitur tidak boleh kosong')
            } else {
                document.querySelector('#form1').submit()
            }
        })
    </script>
    <script></script>
@endsection
