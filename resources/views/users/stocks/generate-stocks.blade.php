@extends('users/templates/index', ['title' => 'Generate Master Stocks'])
@section('content')
<div class="row">
    @if (Session::has('success'))
    <div class="col-12">
        <div class="alert alert-success">{{Session::get('success')}}</div>
    </div>
    @endif
    <div class="row">
        @if (count($stocks) <= 0) <div class="col-12">
            <div class="alert alert-danger">Belum Setup Stock, Silahkan Setup Stock Dibawah</div>
    </div>
    @else
    @foreach ($stocks as $stock)
    <div class="col-6 col-lg-3 col-md-6">
        <div class="card">
            <div class="card-body px-3 py-4-5">
                <div class="row">
                    <div class="col-md-4">
                        <div class="stats-icon purple">
                            <i class="iconly-boldShow"></i>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <h6 class="text-muted font-semibold"> {{$stock->product->name}}</h6>
                        <h6 class="font-extrabold mb-0">b : {{$stock->last_stock}}</h6>
                        <h6 class="font-extrabold mb-0">a : {{$stock->current_stock}}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    @endif
</div>
<div class="row">
    <div class="col-12">
        <div class="card bg-white">
            <div class="card-header">
                <h4 class="card-title">Setup Stock</h4>
                <p>Hanya Diisi Pertama Kali</p>
            </div>
            <div class="card-body">
                <form action="{{route('operator-stock-generate.post')}}" method="post">
                    @csrf
                    <div class="my-3">
                        <label for="">Stock Awal</label>
                        <input type="number" class="form-control" name="last_stock">
                    </div>
                    <div class="my-3">
                        <label for="">Stock Akhir</label>
                        <input type="number" class="form-control" name="current_stock">
                    </div>
                    <div class="my-3">
                        <label for="">Jenis Produk</label>
                        <select name="product_id" class="form-control">
                            @foreach ($products as $product)
                            <option value="{{$product->id}}">{{$product->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="submit" value="Submit" class="btn btn-primary">
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
