@extends('users/templates/index', ['title' => 'Data Buku Tabungan'])
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card bg-white">
            <div class="card-header">
                <h4 class="card-title">Data Buku Tabungan</h4>
                <form action="{{route('operator-saving-books.index')}}" method="get">
                    @csrf
                    <div class="row">
                        <div class="col-3">
                            <select name="product_id" class="form-control">
                                <option value="">--Filter--</option>
                                @foreach ($products as $product)
                                <option value="{{$product->id}}">{{$product->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-3">
                            <input type="submit" value="Filter Produk" class="btn btn-primary">
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Buku</th>
                                    <th>Produk</th>
                                    <th>Cabang</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($books as $book)
                                <tr>
                                    <td>{{$loop->index + 1}}</td>
                                    <td>{{$book->code}}</td>
                                    <td>{{$book->product->name}}</td>
                                    <td>{{$book->branch->name}}</td>
                                    <td>
                                        @if ($book->status == 0)
                                        <div class="badge bg-primary">Tersedia</div>
                                        @else
                                        <div class="badge bg-danger">Sold Out</div>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    let jquery_datatable = $(".table").DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    })
</script>
@endsection
