@extends('manops/templates/index', ['title' => "Pengajuan"])
@section('content')
<section class="row">
    @if (Session::has('success'))
    <div class="row">
        <div class="col-12">
            <div class="alert alert-success">{{Session::get('success')}}</div>
        </div>
    </div>
    @endif
    <div class="col-12">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                       <h4>Request Approval Stock Hilang / Rusak</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="savingBook">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode</th>
                                        <th>Cabang</th>
                                        <th>Tanggal</th>
                                        <th>File</th>
                                        <th>Status Buku</th>
                                        <th>Status Pengajuan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($submissions as $item)
                                    <tr>
                                        <td>{{$loop->index + 1}}</td>
                                        <td>{{$item->savingBook->code}}</td>
                                        <td>{{$item->savingBook->branch->name}}</td>
                                        <td>{{$item->submission_date}}</td>
                                        <td><a href="{{asset('uploads/submission/'.$item->file)}}" class="btn btn-primary btn-sm" download>download</a></td>
                                        <td><div class="badge {{$item->status == 0 ? 'bg-danger' : 'bg-warning'}}">{{$item->status == 0 ? "rusak" : "Hilang"}}</div></td>
                                        <td><div class="badge bg-success">{{$item->submission_status ? "Pending" : "Disetujui"}}</div></td>
                                        <td>
                                            <a href="{{route('manops-approval-savingbook.handleSubmission', ['id' => $item->id, 'savingBookId' => $item->savingBook->id])}}" class="btn btn-primary btn-sm"><i class="bi bi-check"></i></a>
                                            <a href="{{route('manops-approval.destroy', $item->id)}}" class="btn btn-danger btn-sm"><i class="bi bi-trash-fill"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
    $('#savingBook').DataTable()
</script>
@endsection
