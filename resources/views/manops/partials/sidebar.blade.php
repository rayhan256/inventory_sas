<div id="sidebar" class="active">
    <div class="sidebar-wrapper active">
        <div class="sidebar-header">
            <div class="d-flex justify-content-between">
                <div class="logo">
                    <a href="#" style="font-size: 18px;">SAS Inventory - Manops</a>
                </div>
                <div class="toggler">
                    <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                </div>
            </div>
        </div>
        <div class="sidebar-menu">
            <ul class="menu">
                <li class="sidebar-item ">
                    <a href="/manops" class='sidebar-link'>
                        <i class="bi bi-grid-fill"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="sidebar-title">Approval</li>
                <li class="sidebar-item ">
                    <a href="{{route('manops.submission')}}" class='sidebar-link'>
                        <i class="bi bi-person-check-fill"></i>
                        <span class="">Stok Hilang / Rusak</span>
                    </a>
                </li>
                <li class="sidebar-item ">
                    <a href="#" class='sidebar-link'>
                        <i class="bi bi-person-check-fill"></i>
                        <span class="">Pengajuan Stok Buku</span>
                    </a>
                </li>
                <li class="sidebar-title">Setting</li>
                <li class="sidebar-item ">
                    <a href="{{route('auth.signout')}}" onclick="return confirm('anda yakin ingin keluar ? ')" class='sidebar-link'>
                        <i class="bi bi-door-closed-fill"></i>
                        <span class="text-danger">Logout</span>
                    </a>
                </li>
            </ul>
        </div>
        <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
    </div>
</div>
