@extends('manops/templates/index', ['title' => "Dashboard"])
@section('content')
<section class="row">
    @if (Session::has('success'))
    <div class="row">
        <div class="col-12">
            <div class="alert alert-success">{{Session::get('success')}}</div>
        </div>
    </div>
    @endif
    <div class="col-12 my-3">
        <button class="btn btn-primary" onclick="startFCM()"><i class="bi bi-bell">Turn On Notification</i></button>
    </div>
    <div class="col-12">
        <div class="row">
            <div class="col-6 col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body px-3 py-4-5">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="stats-icon purple">
                                    <i class="iconly-boldShow"></i>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h6 class="text-muted font-semibold">KPM</h6>
                                <h6 class="font-extrabold mb-0">Total {{$kpm}}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body px-3 py-4-5">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="stats-icon purple">
                                    <i class="iconly-boldShow"></i>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h6 class="text-muted font-semibold">KPO</h6>
                                <h6 class="font-extrabold mb-0">Total {{$kpo}}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body px-3 py-4-5">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="stats-icon purple">
                                    <i class="iconly-boldShow"></i>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h6 class="text-muted font-semibold">Pemalang</h6>
                                <h6 class="font-extrabold mb-0">Total {{$pemalang}}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body px-3 py-4-5">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="stats-icon purple">
                                    <i class="iconly-boldShow"></i>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h6 class="text-muted font-semibold">Brangsong</h6>
                                <h6 class="font-extrabold mb-0">Total {{$brangsong}}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body px-3 py-4-5">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="stats-icon purple">
                                    <i class="iconly-boldShow"></i>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h6 class="text-muted font-semibold">Mranggen</h6>
                                <h6 class="font-extrabold mb-0">Total {{$mranggen}}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body px-3 py-4-5">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="stats-icon purple">
                                    <i class="iconly-boldShow"></i>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h6 class="text-muted font-semibold">Ambarawa</h6>
                                <h6 class="font-extrabold mb-0">Total {{$ambarawa}}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body px-3 py-4-5">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="stats-icon purple">
                                    <i class="iconly-boldShow"></i>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <h6 class="text-muted font-semibold">Semarang</h6>
                                <h6 class="font-extrabold mb-0">Total {{$semarang}}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                       <h4>Request Approval <i>(Butab)</i></h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="savingBook">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode</th>
                                        <th>Cabang</th>
                                        <th>Tanggal</th>
                                        <th>File</th>
                                        <th>Status Buku</th>
                                        <th>Status Pengajuan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($submissions as $item)
                                    <tr>
                                        <td>{{$loop->index + 1}}</td>
                                        <td>{{$item->savingBook->code}}</td>
                                        <td>{{$item->savingBook->branch->name}}</td>
                                        <td>{{$item->submission_date}}</td>
                                        <td><a href="{{asset('uploads/submission/'.$item->file)}}" class="btn btn-primary btn-sm" download>download</a></td>
                                        <td><div class="badge {{$item->status == 0 ? 'bg-danger' : 'bg-warning'}}">{{$item->status == 0 ? "rusak" : "Hilang"}}</div></td>
                                        <td><div class="badge bg-success">{{$item->submission_status ? "Pending" : "Disetujui"}}</div></td>
                                        <td>
                                            <a href="{{route('manops-approval-savingbook.handleSubmission', ['id' => $item->id, 'savingBookId' => $item->savingBook->id])}}" class="btn btn-primary btn-sm"><i class="bi bi-check"></i></a>
                                            <a href="{{route('manops-approval.destroy', $item->id)}}" class="btn btn-danger btn-sm"><i class="bi bi-trash-fill"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Grafik Jumlah Stock Cabang</h4>
                    </div>
                    <div class="card-body">
                        <div id="chart-profile-visit"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('js')
<script>
    $('#savingBook').DataTable()
</script>
<script>
    var optionsProfileVisit = {
	annotations: {
		position: 'back'
	},
	dataLabels: {
		enabled:false
	},
	chart: {
		type: 'bar',
		height: 300
	},
	fill: {
		opacity:1
	},
	plotOptions: {
	},
	series: [{
		name: 'cabang',
		data: [+'{{$kpm}}',+'{{$kpo}}',+'{{$pemalang}}',+'{{$mranggen}}',+'{{$brangsong}}',+'{{$ambarawa}}',+'{{$semarang}}']
	}],
	colors: '#435ebe',
	xaxis: {
		categories: ["KPM","KPO","Pemalang","Mranggen","Brangsong","Ambarawa","Semarang"],
	},
}



var chartProfileVisit = new ApexCharts(document.querySelector("#chart-profile-visit"), optionsProfileVisit);
chartProfileVisit.render();

</script>
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase.js"></script>
<script>
     const firebaseConfig = {
      apiKey: "AIzaSyDSOZ69abhtwYDPWaTRpWCahORwY4T9jCA",
      authDomain: "inventory-sas.firebaseapp.com",
      projectId: "inventory-sas",
      storageBucket: "inventory-sas.appspot.com",
      messagingSenderId: "400732378158",
      appId: "1:400732378158:web:1ad5425d282f65e0e1bbbf",
      measurementId: "G-SSSFPL5XWV"
    };
    firebase.initializeApp(firebaseConfig);
    const messaging = firebase.messaging();

    function startFCM() {
        messaging
            .requestPermission()
            .then(function () {
                return messaging.getToken()
            })
            .then(function (response) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ route("store.token") }}',
                    type: 'POST',
                    data: {
                      device_token: response
                    },
                    dataType: 'JSON',
                    success: function (response) {
                        alert('Notifikasi Dinyalakan');
                    },
                    error: function (error) {
                        alert("error");
                    },
                });

            }).catch(function (error) {
                alert("error");
            });
    }

    messaging.onMessage(function (payload) {
        const title = payload.notification.title;
        const options = {
            body: payload.notification.body,
            icon: "{{asset('assets/images/sas-logo.png')}}",
        };
        new Notification(title, options);
    });
</script>
@endsection
