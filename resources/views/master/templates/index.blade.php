<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard - Inventaris Master</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('master/partials/styles')
</head>

<body>
    <div id="app">
        @include('master/partials/sidebar')
        <div id="main">
            <header class="mb-3">
                <a href="#" class="burger-btn d-block d-xl-none">
                    <i class="bi bi-justify fs-3"></i>
                </a>
            </header>

            <div class="page-heading">
                <h3>{{ $title }}</h3>
            </div>
            <div class="page-content">
                @yield('content')
            </div>

            <footer>
                <div class="footer clearfix mb-0 text-muted">
                    <div class="float-start">
                        <p>2021 &copy; PT BPR Sejahtera Artha Sembada V0.1 Beta</p>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    @include('master/partials/scripts')
    @yield('js')
</body>

</html>
