@extends('master/templates/index', ['title' => 'Manajemen Stock'])
@section('content')
    <div class="row">
        @if (Session::has('error'))
            <div class="col-12">
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            </div>
        @endif
        @if (Session::has('success'))
            <div class="col-12">
                <div class="alert alert-success">{{ Session::get('success') }}</div>
            </div>
        @endif
        <div class="col-12">
            <div class="row">
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon purple">
                                        <i class="iconly-boldShow"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">KPM</h6>
                                    <h6 class="font-extrabold mb-0">{{ $kpm }}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon purple">
                                        <i class="iconly-boldShow"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">KPO</h6>
                                    <h6 class="font-extrabold mb-0">{{ $kpo }}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon purple">
                                        <i class="iconly-boldShow"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">Pemalang</h6>
                                    <h6 class="font-extrabold mb-0">{{ $pemalang }}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon purple">
                                        <i class="iconly-boldShow"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">Brangsong</h6>
                                    <h6 class="font-extrabold mb-0">{{ $brangsong }}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon purple">
                                        <i class="iconly-boldShow"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">Mranggen</h6>
                                    <h6 class="font-extrabold mb-0">{{ $mranggen }}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon purple">
                                        <i class="iconly-boldShow"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">Ambarawa</h6>
                                    <h6 class="font-extrabold mb-0">{{ $ambarawa }}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 col-lg-3 col-md-6">
                    <div class="card">
                        <div class="card-body px-3 py-4-5">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="stats-icon purple">
                                        <i class="iconly-boldShow"></i>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <h6 class="text-muted font-semibold">Semarang</h6>
                                    <h6 class="font-extrabold mb-0">{{ $semarang }}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Data Jumlah Stock Per Cabang</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Stock Awal</th>
                                    <th>Stock Akhir</th>
                                    <th>Produk</th>
                                    <th>Cabang</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($saving_book_details as $detail)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $detail->last_stock }}</td>
                                        <td>{{ $detail->current_stock }}</td>
                                        <td>{{ $detail->product->name }}</td>
                                        <td>{{ $detail->branch->name }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Stok Masuk</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('master-stock.incomesPost') }}" method="post">
                        @csrf
                        <div class="my-3">
                            <input type="number" name="count" placeholder="Jumlah Masuk" id=""
                                class="form-control">
                        </div>
                        <div class="my-3">
                            <input type="date" name="date_in" class="form-control">
                        </div>
                        <div class="my-3">
                            <select name="product_id" class="form-control" id="product_id_in">
                                <option value="">Pilih Jenis Buku Tabungan</option>
                                @foreach ($products as $product)
                                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="my-3 row d-none" id="custome_code">
                            <div class="col-6">
                                <label for="">Kode Buku Mulai Dari</label>
                                <input type="text" name="book_code_lowest" id="saving_book_code_highest"
                                    class="form-control">
                            </div>
                            <div class="col-6">
                                <label for="">Hingga</label>
                                <input type="text" name="book_code_highest" id="saving_book_code_lowest"
                                    class="form-control">
                            </div>
                        </div>
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <h3 class="my-3">Pembagian Stok Antar Cabang</h3>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Form Pembagian Stock</h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('master-stock.toBranch') }}" method="post">
                        @csrf
                        <div class="my-3">
                            <label for="">Jenis Produk</label>
                            <select name="product_id" class="form-control">
                                <option value="">== Pilih Produk ==</option>
                                @foreach ($products as $product)
                                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="my-3">
                            <label for="">Tanggal</label>
                            <input type="date" name="date" id="" class="form-control">
                        </div>
                        <div class="my-3">
                            <label for="">Cabang</label>
                            <select name="branch_id" id="" class="form-control">
                                <option value="">== Pilih Cabang ==</option>
                                @foreach ($branches as $branch)
                                    <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="my-3 row">
                            <div class="col-6">
                                <label for="">Kode Buku Mulai Dari</label>
                                <input type="text" name="book_code_lowest" id="saving_book_code_highest"
                                    class="form-control">
                            </div>
                            <div class="col-6">
                                <label for="">Hingga</label>
                                <input type="text" name="book_code_highest" id="saving_book_code_lowest"
                                    class="form-control">
                            </div>
                        </div>
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.table').DataTable()
    </script>
    <script>
        let data = [];
    </script>
    @foreach ($saving_books as $book)
        <script>
            data.push('{{ $book->code }}')
        </script>
    @endforeach
    <script>
        let customeCode = document.getElementById('custome_code')
        let productIdIn = document.getElementById('product_id_in')

        productIdIn.addEventListener('change', (e) => {
            if (productIdIn.value == 5) {
                customeCode.classList.remove('d-none')
            } else {
                customeCode.classList.add('d-none')
            }
        })

        $(function() {
            $('#saving_book_code_highest').autocomplete({
                source: function(request, response) {
                    var results = $.ui.autocomplete.filter(data, request.term);
                    response(results.slice(0, 10));
                }
            })
            $('#saving_book_code_lowest').autocomplete({
                source: function(request, response) {
                    var results = $.ui.autocomplete.filter(data, request.term);
                    response(results.slice(0, 10));
                }
            })
        })
    </script>
@endsection
