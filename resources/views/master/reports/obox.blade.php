@extends('master/templates/index', ['title' => 'Generate Obox Report'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Setup Header Table</h4>
                </div>
                <div class="card-body">
                    <form action="{{route('master-obox.setupHeader')}}" method="post">
                        @csrf
                        <div class="my-3">
                            <label for="">Flag Header</label>
                            <input type="text" name="flag_header" value="H" class="form-control" readonly>
                        </div>
                        <div class="my-3">
                            <label for="">Tipe LJK</label>
                            <input type="text" name="ljk_type" class="form-control" value="01020101" readonly>
                        </div>
                        <div class="my-3">
                            <label for="">Tipe Data</label>
                            <input type="text" name="datatype" value="A" class="form-control" readonly>
                        </div>
                        <div class="my-3">
                            <label for="">Kode LJK</label>
                            <input type="text" name="ljk_code" class="form-control">
                        </div>
                        <div class="my-3">
                            <label for="">Tanggal Periode</label>
                            <input type="date" name="period" id="" class="form-control">
                        </div>
                        <div class="my-3">
                            <label for="">Jumlah Data</label>
                            <input type="number" min="0" name="data_count" class="form-control">
                        </div>
                        <input type="submit" value="Setup Header Table" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h4>Tabel Stock Report</h4>
                    <form action="#" method="get">
                        <div class="d-flex">
                            <select name="branch_id" id="" class="form-control">
                                <option value="">Pilih Cabang</option>
                                @foreach ($branch as $b)
                                <option value="{{$b->id}}">{{$b->name}}</option>
                                @endforeach
                            </select>
                            <input type="submit" value="Filter" class="btn btn-secondary">
                        </div>
                    </form>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Cabang</th>
                                    <th>Stock Awal</th>
                                    <th>Tambahan Stock</th>
                                    <th>Stock Terpakai</th>
                                    <th>Stock Rusak</th>
                                    <th>Stock Hilang</th>
                                    <th>Stock Akhir</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>KPO</td>
                                    <td>20 Stock</td>
                                    <td>10 Stock</td>
                                    <td>5 Stock</td>
                                    <td>1 Stock</td>
                                    <td>1 Stock</td>
                                    <td>15 Stock</td>
                                    <td><a href="#" class="btn btn-primary btn-sm"><i class="bi bi-arrow-down"></i></a></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>KPO</td>
                                    <td>20 Stock</td>
                                    <td>10 Stock</td>
                                    <td>5 Stock</td>
                                    <td>1 Stock</td>
                                    <td>1 Stock</td>
                                    <td>15 Stock</td>
                                    <td><a href="#" class="btn btn-primary btn-sm"><i class="bi bi-arrow-down"></i></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection