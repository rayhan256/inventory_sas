@extends('master/templates/index', ['title' => 'General Reports'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-primary my-3" href="{{ route('master-reports.general-export') }}">Export Excel</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>No Rekening</th>
                                    <th>Alamat</th>
                                    <th>Cabang</th>
                                    <th>No Buku</th>
                                    <th>Waktu</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($reports as $report)
                                    @foreach ($report->stockOut as $item)
                                        <tr>
                                            <td>{{ $item->debtor->name }}</td>
                                            <td>{{ $item->debtor->account_id }}</td>
                                            <td>{{ $item->debtor->address }}</td>
                                            <td>{{ $report->branch->name }}</td>
                                            <td>{{ $item->savingBook->code ?? '' }}</td>
                                            <td>{{ $item->created_at }}</td>
                                            <td><a href="{{ route('master-report-out.edit', $item->id) }}"
                                                    class="btn btn-primary">Ubah</a></td>
                                        </tr>
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.table').dataTable()
    </script>
@endsection
