@extends('master/templates/index', ['title' => 'Ubah Data Stok Keluar'])
@section('content')
    <div class="row">
        @if (Session::has('success'))
        <div class="col-12">
            <div class="alert alert-success">
                {{Session::get('success')}}
            </div>
        </div>
        @endif
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Form Update Data Stock Keluar</h4>
                </div>
                <div class="card-body">
                    <form action="{{route('master-stock-report.edit')}}" method="post">
                        @csrf
                        <input type="hidden" value="{{$report->id}}" name="id">
                        <div class="my-3 row">
                            <div class="col-6">
                                <label for="">No Rekening</label>
                                <input type="text" class="form-control" value="{{$report->debtor->account_id}}" disabled>
                            </div>
                            <div class="col-6">
                                <label for="">Nama Nasabah</label>
                                <input type="text" class="form-control" value="{{$report->debtor->name}}" disabled>
                            </div>
                        </div>
                        <div class="my-3">
                            <label for="">Kode Sebelumnya</label>
                            <input type="text" name="old_book_id" class="form-control" value="{{$report->savingBook->code}}" readonly>
                        </div>
                        <div class="my-3">
                            <label for="">Ubah Buku</label>
                            <input type="text" name="saving_book_id" id="saving_book_id" class="form-control">
                        </div>
                        <input type="submit" value="Submit" class="bnt btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    let data = []
</script>
@foreach ($saving_books as $book)
    <script>
        data.push('{{$book->code}}')
    </script>
@endforeach
<script>
     $('#saving_book_id').autocomplete({
                source: function (request, response) {
                    var results = $.ui.autocomplete.filter(data, request.term);
                    response(results.slice(0, 10));
                }
     })
</script>
@endsection