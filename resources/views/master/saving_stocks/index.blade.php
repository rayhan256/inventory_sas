@extends('master/templates/index', ['title' => 'Data Stock Buku Seluruh Cabang'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Tabel Data Seluruh Stock</h4>
                    <form action="{{ route('master-savingbook.index') }}" method="get">
                        @csrf
                        <div class="my-3">
                            <select name="branch_id" id="" class="form-control">
                                <option value="">Pilih Cabang</option>
                                @foreach ($branches as $branch)
                                    <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="my-3">
                            <input type="submit" value="Filter" class="btn btn-primary">
                        </div>
                    </form>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kode Buku</th>
                                    <th>Produk</th>
                                    <th>Cabang</th>
                                    <th>Status</th>
                                    <th>Serah Terima Cabang</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($books as $book)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $book->code }}</td>
                                        <td>{{ $book->product->name }}</td>
                                        <td>{{ $book->branch->name }}</td>
                                        <td>
                                            @switch($book->status)
                                                @case(0)
                                                    <div class="badge bg-primary">Tersedia</div>
                                                @break

                                                @case(1)
                                                    <div class="badge bg-danger">Sold Out</div>
                                                @break

                                                @case(2)
                                                    <div class="badge bg-danger">Hilang</div>
                                                @break

                                                @case(3)
                                                    <div class="badge bg-danger">Rusak</div>
                                                @break

                                                @default
                                                    <div class="badge bg-primary">Tersedia</div>
                                            @endswitch
                                        </td>
                                        <td>
                                            {{ $book->kpm_status == 0 ? 'Belum' : 'Sudah' }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>
                        {{ $books->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
{{-- @section('js')
    <script>
        let dataTable = $('.table').DataTable()
    </script>
@endsection --}}
