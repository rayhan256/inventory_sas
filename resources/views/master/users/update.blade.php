@extends('master/templates/index', ['title' => 'Ubah Pengguna'])
@section('content')
    <div class="row">
        <div class="">
            <div class="card">
                <div class="card-header">
                    <h4>Ubah Pengguna</h4>
                </div>
                <div class="card-body">
                    <form action="{{route('master-user.edit')}}" method="post">
                        @csrf
                        <input type="hidden" value="{{$user->id}}" name="id">
                        <div class="my-3">
                            <input type="email" class="form-control" name="email" value="{{$user->email}}" placeholder="Masukkan Email">
                        </div>
                        <div class="my-3">
                            <input type="text" class="form-control" name="name" value="{{$user->name}}" placeholder="Masukkan Nama Pengguna">
                        </div>
                        <div class="my-3">
                            <select name="role" id="" class="form-control">
                                <option value="{{$user->role}}">{{$user->role == 0 ? "Master (current)" : "Operator (current)"}}</option>
                                <option value="0">Master</option>
                                <option value="1">Operator</option>
                            </select>
                        </div>
                        <div class="my-3">
                            <select name="branch_id" id="" class="form-control">
                               @if (isset($user->detail->branch))
                               <option value="{{$user->detail->branch->id}}">{{$user->detail->branch->name}} (Current)</option>
                               @endif
                                @foreach ($branches as $branch)
                                    <option value="{{$branch->id}}">{{$branch->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="submit" value="Ubah" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection