@extends('master/templates/index', ['title' => 'Tambah Pengguna'])
@section('content')
    <div class="row">
        <div class="">
            <div class="card">
                <div class="card-header">
                    <h4>Pilih Cabang Asal</h4>
                    <div class="text-center">
                        <img src="{{asset('assets/images/progress-success.png')}}" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{route('master-user.post-detail')}}" method="post">
                        @csrf
                        <input type="hidden" value="{{$id}}" name="user_id">
                       <div class="my-3">
                        <select name="branch_id" class="form-control" id="">
                            <option value="">== Pilih Cabang ==</option>
                            @foreach ($branches as $branch)
                                <option value="{{$branch->id}}">{{$branch->name}}</option>
                            @endforeach
                        </select>
                       </div>
                        <input type="submit" value="Tambah" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection