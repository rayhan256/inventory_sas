@extends('master/templates/index', ['title' => 'Tambah Pengguna'])
@section('content')
    <div class="row">
        <div class="">
            <div class="card">
                <div class="card-header">
                    <h4>Tambah Pengguna</h4>
                    <div class="text-center">
                        <img src="{{asset('assets/images/progress.png')}}" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{route('master-user.post')}}" method="post">
                        @csrf
                        <div class="my-3">
                            <input type="email" class="form-control" name="email" placeholder="Masukkan Email">
                        </div>
                        <div class="my-3">
                            <input type="text" class="form-control" name="name" placeholder="Masukkan Nama Pengguna">
                        </div>
                        <div class="my-3">
                            <input type="password" class="form-control" name="password" placeholder="Masukkan Password">
                        </div>
                        <div class="my-3">
                            <select name="role" id="" class="form-control">
                                <option value="">==Pilih Role==</option>
                                <option value="0">Master</option>
                                <option value="1">Operator</option>
                                <option value="2">Manops</option>
                            </select>
                        </div>
                        <input type="submit" value="Next" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection