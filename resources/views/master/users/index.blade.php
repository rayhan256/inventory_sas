@extends('master/templates/index', ['title' => 'User Management'])
@section('content')
    <div class="row">
        <div class="col-12">
            @if (Session::has('success'))
            <div class="alert alert-success">{{Session::get('success')}}</div>
            @endif
            <div class="card">
                <div class="card-header">
                    <h4>Tabel Data Pengguna</h4>
                    <a href="{{route('master-user.add')}}" class="btn btn-primary my-3">Tambah</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Cabang</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                <tr>
                                    <td>{{$loop->index + 1}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->detail->branch->name ?? "-"}}</td>
                                    <td>
                                        <a href="{{route('master-user.editView', $user->id)}}" class="btn btn-primary">Ubah</a>
                                        <a href="{{route('master-user.destroy', $user->id)}}" class="btn btn-danger">Hapus</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        let jquery_datatable = $(".table").DataTable()
    </script>
@endsection