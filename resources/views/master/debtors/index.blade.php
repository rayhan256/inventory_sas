@extends('master/templates/index', ['title' => 'Nasabah Seluruh Cabang'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                   <h4>Tabel Data Seluruh Nasabah BPR SAS</h4>
                   <form action="{{route('master-debtor.index')}}" method="get">
                       @csrf
                      <div class="my-3">
                        <select name="branch_id" id="" class="form-control">
                            <option value="">Pilih Cabang</option>
                            @foreach ($branches as $branch)
                                <option value="{{$branch->id}}">{{$branch->name}}</option>
                            @endforeach
                        </select>
                      </div>
                      <div class="my-3">
                          <input type="submit" value="Filter" class="btn btn-primary">
                      </div>
                   </form>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No Rekening</th>
                                    <th>Nama</th>
                                    <th>NIK</th>
                                    <th>Cabang</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($debtors as $debtor)
                                <tr>
                                    <td>{{$debtor->account_id}}</td>
                                    <td>{{$debtor->name}}</td>
                                    <td>{{$debtor->nik}}</td>
                                    <td>{{$debtor->branch->name}}</td>    
                                    <td>
                                        <a href="{{route('master-debtors.edit', $debtor->id)}}" class="btn btn-primary">Ubah</a>
                                        <a href="{{route('master-debtors.destroy', $debtor->id)}}}}" class="btn btn-danger" onclick="return confirm('Anda Yakin Ingin Mengapus Nasabah Ini ?')">Hapus</a>
                                    </td>                              
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
       let dataTable =  $('.table').DataTable()
    </script>
@endsection