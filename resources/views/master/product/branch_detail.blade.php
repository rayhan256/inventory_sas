@extends('master/templates/index', ['title' => 'Detail Cabang'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <h4>Tambah Produk Cabang {{ $branch->name }}</h4>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{ route('master-product.add_stock') }}" method="post">
                        @csrf
                        <input type="hidden" name="branch_id" value="{{ $branch->id }}">
                        <div class="form-group">
                            <label for="">Produk</label>
                            <select name="product_id" id="" class="form-control">
                                @foreach ($products as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
