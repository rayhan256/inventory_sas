@extends('master/templates/index', ['title' => 'Tambah Produk'])
@section('content')
    <div class="row">
        @if (Session::has('success'))
            <div class="col-12">
                <div class="alert alert-success">{{ Session::get('success') }}</div>
            </div>
        @endif
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('master-product.create') }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="">Nama Produk</label>
                            <input type="text" class="form-control" name="name">
                        </div>
                        <div class="form-group">
                            <label for="">Prefix</label>
                            <input type="text" class="form-control" name="prefix">
                        </div>
                        <div class="form-group">
                            <label for="">Kode Produk</label>
                            <input type="text" class="form-control" name="code">
                        </div>
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
