<div id="sidebar" class="active">
    <div class="sidebar-wrapper active">
        <div class="sidebar-header">
            <div class="d-flex justify-content-between">
                <div class="logo">
                    <a href="/" style="font-size: 18px;">SAS Inventory - Master</a>
                </div>
                <div class="toggler">
                    <a href="#" class="sidebar-hide d-xl-none d-block"><i class="bi bi-x bi-middle"></i></a>
                </div>
            </div>
        </div>
        <div class="sidebar-menu">
            <ul class="menu">
                <li class="sidebar-item ">
                    <a href="/master" class='sidebar-link'>
                        <i class="bi bi-grid-fill"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="sidebar-title">Menu</li>
                <li class="sidebar-item  has-sub">
                    <a href="#" class='sidebar-link'>
                        <i class="bi bi-stack"></i>
                        <span>Data Master</span>
                    </a>
                    <ul class="submenu ">
                        <li class="submenu-item ">
                            <a href="{{ route('master-debtor.index') }}">Nasabah</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="{{ route('master-savingbook.index') }}">Stok Buku</a>
                        </li>
                        <li class="submenu-item ">
                            <a href="{{ route('master-product.index') }}">Produk</a>
                        </li>
                    </ul>
                </li>

                <li class="sidebar-title">Manajemen Stok</li>
                <li class="sidebar-item ">
                    <a href="{{ route('master-stock.index') }}" class='sidebar-link'>
                        <i class="bi bi-file-zip-fill"></i>
                        <span>Kontrol Stok</span>
                    </a>
                    <a href="{{ route('master-stock-history.index') }}" class='sidebar-link'>
                        <i class="bi bi-file-zip-fill"></i>
                        <span>Riwayat</span>
                    </a>
                </li>
                <li class="sidebar-title">Laporan</li>

                <li class="sidebar-item  ">
                    <a href="{{ route('master-reports.general') }}" class='sidebar-link'>
                        <i class="bi bi-grid-1x2-fill"></i>
                        <span>Laporan Umum</span>
                    </a>
                    {{-- <a href="{{route('master-obox.index')}}" class='sidebar-link'>
                        <i class="bi bi-archive-fill"></i>
                        <span>OBOX</span>
                    </a> --}}
                </li>
                <li class="sidebar-title">Setting</li>
                <li class="sidebar-item ">
                    <a href="{{ route('master-user.index') }}" class='sidebar-link'>
                        <i class="bi bi-person-check-fill"></i>
                        <span class="">Manajemen Pengguna</span>
                    </a>
                </li>
                <li class="sidebar-item ">
                    <a href="{{ route('auth.signout') }}" onclick="return confirm('anda yakin ingin keluar ? ')"
                        class='sidebar-link'>
                        <i class="bi bi-door-closed-fill"></i>
                        <span class="text-danger">Logout</span>
                    </a>
                </li>
            </ul>
        </div>
        <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
    </div>
</div>
