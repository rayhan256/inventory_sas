@extends('master/templates/index', ['title' => 'History Stock Keluar'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h4>Tabel History Stock Keluar</h4>
                    <a href="{{route('master-stock-history.index')}}" class="btn btn-primary">Switch To Stock In</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Jumlah</th>
                                    <th>Tanggal</th>
                                    <th>Cabang</th>
                                    <th>Produk</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($histories as $history)
                                    <tr>
                                        <td>{{$loop->index + 1}}</td>
                                        <td>{{$history->count}}</td>
                                        <td>{{$history->date_out}}</td>
                                        <td>{{$history->stock->branch->name}}</td>
                                        <td>{{$history->stock->product->name}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.table').DataTable()
    </script>
@endsection