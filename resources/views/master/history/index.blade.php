@extends('master/templates/index', ['title' => 'History Stock Masuk'])
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h4>Tabel Riwayat Stock Masuk</h4>
                    <a href="{{ route('master-stock-history.stockout') }}" class="btn btn-primary">Switch To Stock Out</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Jumlah Masuk</th>
                                    <th>Stock Akhir</th>
                                    <th>Tanggal Masuk</th>
                                    <th>Produk</th>
                                    <th>Cabang</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($histories as $history)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ $history->count }}</td>
                                        <td>{{ $history->last_stock ?? 'null' }}</td>
                                        <td>{{ $history->date_in }}</td>
                                        <td>{{ $history->stock->product->name }}</td>
                                        <td>{{ $history->stock->branch->name }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $('.table').DataTable()
    </script>
@endsection
