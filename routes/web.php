<?php

use App\Http\Controllers\Api\MasterController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Manops\DashboardController as ManopsDashboardController;
use App\Http\Controllers\Manops\SubmissionRequestController;
use App\Http\Controllers\Master\AprovalController;
use App\Http\Controllers\Master\DashboardController;
use App\Http\Controllers\Master\DebtorController as MasterDebtorController;
use App\Http\Controllers\Master\HistoryController;
use App\Http\Controllers\Master\OboxReportController;
use App\Http\Controllers\Master\ProductController;
use App\Http\Controllers\Master\SavingBookController as MasterSavingBookController;
use App\Http\Controllers\Master\StockController as MasterStockController;
use App\Http\Controllers\Master\StockReportController;
use App\Http\Controllers\Master\UserManagementController;
use App\Http\Controllers\Operator\BilyetSubmissionController;
use App\Http\Controllers\Operator\DashboardController as OperatorDashboardController;
use App\Http\Controllers\Operator\DebtorController;
use App\Http\Controllers\Operator\ExportPdfController;
use App\Http\Controllers\Operator\ReportController;
use App\Http\Controllers\Operator\SavingBookController;
use App\Http\Controllers\Operator\StockController;
use App\Http\Controllers\Operator\SubmissionController;
use App\Http\Controllers\WebNotificationController;
use App\Models\BilyetSubmission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware(['guest'])->name('auth.login');
Route::get('/books/{product_id}', [MasterController::class, 'getSavingBookBranch'])->name('master.branchSavingBooks');
Route::get('/debtors/{nik}', [MasterController::class, 'getDebtorByNik'])->name('master.debtorApi');
Route::get('/books', [StockReportController::class, 'getAllSavingBook'])->name('master.books');

Route::middleware(['auth'])->group(function () {
    Route::get('/push-notificaiton', [WebNotificationController::class, 'index'])->name('push-notificaiton');
    Route::post('/store-token', [WebNotificationController::class, 'storeToken'])->name('store.token');
    Route::post('/send-web-notification', [WebNotificationController::class, 'sendWebNotification'])->name('send.web-notification');
    Route::get('/logout', [AuthController::class, 'signout'])->name('auth.signout');
});

Route::prefix('master')->middleware(['auth', 'master'])->group(function () {
    // Dashboard
    Route::get('/', [DashboardController::class, 'index']);

    // users
    Route::get('/users', [UserManagementController::class, 'index'])->name('master-user.index');
    Route::get('/users/add', [UserManagementController::class, 'add'])->name('master-user.add');
    Route::get('/users/add/{id}', [UserManagementController::class, 'addBranch'])->name('master-user.add-branch');
    Route::post('/users/post', [UserManagementController::class, 'post'])->name('master-user.post');
    Route::post('/users/postDetail', [UserManagementController::class, 'postDetail'])->name('master-user.post-detail');
    Route::get('/users/{id}', [UserManagementController::class, 'editView'])->name('master-user.editView');
    Route::post('/users/update', [UserManagementController::class, 'edit'])->name('master-user.edit');
    Route::get('/users/destroy/{id}', [UserManagementController::class, 'destroy'])->name('master-user.destroy');

    // debtors
    Route::get('/debtors', [MasterDebtorController::class, 'index'])->name('master-debtor.index');
    Route::get('/debtors/{id}', [MasterDebtorController::class, 'editView'])->name('master-debtors.edit');
    Route::post('/debtors/update', [MasterDebtorController::class, 'edit'])->name('master-debtors.update');
    Route::get('/debtors/destroy/{id}', [MasterDebtorController::class, 'destroy'])->name('master-debtors.destroy');

    // saving book
    Route::get('/saving-books', [MasterSavingBookController::class, 'index'])->name('master-savingbook.index');

    // Stock Control
    Route::get('/stock-controls', [MasterStockController::class, 'index'])->name('master-stock.index');
    Route::post('/stocks/incomes/post', [MasterStockController::class, 'postStockIn'])->name('master-stock.incomesPost');
    Route::post('/stocks/bilyets/post', [MasterStockController::class, 'postBilyetIn'])->name('master-stock.incomeBilyets');
    Route::post('/stocks/kpmtobranchs', [MasterStockController::class, 'postBranchStock'])->name('master-stock.toBranch');

    // Aproval
    Route::get('/approvals/saving_books/{id}/{savingBookId}', [AprovalController::class, 'handleSavingBookSubmission'])->name('master-approval-savingbook.handleSubmission');
    Route::get('/approvals/bilyets/{id}/{bilyetId}', [AprovalController::class, 'handleBilyetSubmission'])->name('master-approval-bilyet.handleSubmission');

    // reports
    Route::get('/reports', [StockReportController::class, 'index'])->name('master-reports.general');
    Route::get('/reprots/general', [StockReportController::class, 'exportGeneralReport'])->name('master-reports.general-export');
    Route::get('/reports/out/edit/{id}', [StockReportController::class, 'detail'])->name('master-report-out.edit');

    // Obox Report
    Route::get('/obox', [OboxReportController::class, 'index'])->name('master-obox.index');
    Route::post('/obox/headerSetup', [OboxReportController::class, 'postOboxHeaderTable'])->name('master-obox.setupHeader');

    // History
    Route::get('/stocks/histories', [HistoryController::class, 'index'])->name('master-stock-history.index');
    Route::get('/stocks/histories/out', [HistoryController::class, 'stockOut'])->name('master-stock-history.stockout');
    Route::post('/stocks/out/edit', [StockReportController::class, 'edit'])->name('master-stock-report.edit');

    // produk
    Route::get('/products', [ProductController::class, 'index'])->name('master-product.index');
    Route::get('/products/insert', [ProductController::class, 'create'])->name('master-product.insert');
    Route::post('/products/create', [ProductController::class, 'insertProduct'])->name('master-product.create');
    Route::get('/products/branch_detail/{branch_id}', [ProductController::class, 'branchDetail'])->name('master-product.branch_detail');
    Route::post('/products/branch_detail/insert', [ProductController::class, 'storeBranchProduct'])->name('master-product.add_stock');
});

// Dashboard
Route::get('operator/', [OperatorDashboardController::class, 'index'])->middleware(['auth']);

Route::prefix('operator')->middleware(['auth', 'operator'])->group(function () {

    // Nasabah
    Route::get('/debtors', [DebtorController::class, 'index'])->name('operator-debtors.index');
    Route::get('/debtors/add', [DebtorController::class, 'add'])->name('operator-debtors.add');
    Route::get('/debtors/{id}', [DebtorController::class, 'editView'])->name('operator-debtors.edit');
    Route::post('/debtors/add', [DebtorController::class, 'post'])->name('operator-debtors.post');
    Route::post('/debtors/update', [DebtorController::class, 'edit'])->name('operator-debtors.update');
    Route::get('/debtors/destroy/{id}', [DebtorController::class, 'destroy'])->name('operator-debtors.destroy');

    // Stock Masuk
    Route::get('/stocks', [StockController::class, 'index'])->name('operator-stock.index');
    Route::get('/stocks/generates/', [StockController::class, 'generateStock'])->name('operator-stock.generate');
    Route::post('/stocks/generates', [StockController::class, 'postMasterStock'])->name('operator-stock-generate.post');
    Route::post('/stocks/outs', [StockController::class, 'postStockOut'])->name('operator-stock.outPost');
    Route::post('/stocks/bilyet/out', [StockController::class, 'postBilyetOut'])->name('operator-stock.bilyetOut');
    Route::post('/stocks/outs/manual', [StockController::class, 'postManualStockOut'])->name('operator-stock.outPostManual');


    // saving books
    Route::get('/saving-books', [SavingBookController::class, 'index'])->name('operator-saving-books.index');

    // submission
    Route::get('/submissions', [SubmissionController::class, 'index'])->name('operator-submission.index');
    Route::post('/submission/post', [SubmissionController::class, 'postSubmission'])->name('operator-submission.post');

    // Bilyet Submission
    Route::get('/submissions/bilyets', [BilyetSubmissionController::class, 'index'])->name('operator-submission-bilyet.index');
    Route::post('/submissions/bilyets/post', [BilyetSubmissionController::class, 'postSubmission'])->name('operator-submission-bilyet.post');

    // Report
    Route::get('/reports', [ReportController::class, 'index'])->name('operator-report.index');
    Route::get('/reports/excel', [ReportController::class, 'export'])->name('operator-report.export');
    Route::get('/reports/export', [ExportPdfController::class, 'index'])->name('operator-report.export-pdf');
});


// Manops dashboard
Route::prefix('manops')->middleware(['auth', 'manops'])->group(function () {
    Route::get('/', [ManopsDashboardController::class, 'index'])->name('manops');
    Route::get('/submissions', [SubmissionRequestController::class, 'index'])->name('manops.submission');
    Route::get('/approvals/saving_books/{id}/{savingBookId}', [SubmissionRequestController::class, 'handleSavingBookSubmission'])->name('manops-approval-savingbook.handleSubmission');
    Route::get('/approvals/destroy/{id}', [SubmissionRequestController::class, 'handleDestroySubmission'])->name('manops-approval.destroy');
});
